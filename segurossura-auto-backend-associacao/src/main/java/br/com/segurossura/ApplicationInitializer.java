package br.com.segurossura;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class ApplicationInitializer  extends SpringBootServletInitializer {

	@Value("${api.contato.name}")
	private String contactName;
	
	@Value("${api.contato.email}")
	private String contactEmail;

	public static void main(String[] args) {
		SpringApplication.run(ApplicationInitializer.class, args);
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ApplicationInitializer.class);
    }
	
	/**
	 * swaggerSpringMvcPlugin
	 * @return Docket return
	 */
	
	@Bean
	public Docket swaggerSpringMvcPlugin() {
		return new Docket(DocumentationType.SWAGGER_2)
				.useDefaultResponseMessages(false)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
				.build();
	}

	/**
	 * apiInfo
	 * @return ApiInfo
	 */
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("api_assoc")
				.description("api_assoc")
				.contact(new Contact(contactName, null, contactEmail))
				.version("1.0-SNAPSHOT")
				.build();
	}
}
