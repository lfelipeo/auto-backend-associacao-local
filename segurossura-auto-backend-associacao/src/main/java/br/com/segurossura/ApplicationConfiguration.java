package br.com.segurossura;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import br.com.segurossura.auto.security.JwtFilter;


@Configuration
public class ApplicationConfiguration {
	
	@Autowired
	private JwtFilter jwtFilter;
	
	/**
	 * corsFilter 
	 * 
	 * @return retornar o filtro de registro do Bean
	 */
	
	@Bean
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("OPTIONS");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("DELETE");
        config.addAllowedMethod("HEAD");
        config.addAllowedMethod("TRACE");
        config.addAllowedMethod("CONNECT");
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }
		
	/**
	 * securityFilterChainRegistration
	 * @return retornar o filtro de registro do Bean
	 */
	
	@Bean
    public FilterRegistrationBean securityFilterChainRegistration() {
	    FilterRegistrationBean registrationBean = new FilterRegistrationBean(jwtFilter);
	    registrationBean.addUrlPatterns("/api/v1/*");
	    return registrationBean;
    }
	
	/**
	 * sessionFactory
	 * @return retorna o bean da sess�o do hibernate
	 */
	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory() {
		return new HibernateJpaSessionFactoryBean();
	}


}
