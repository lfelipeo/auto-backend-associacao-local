package br.com.segurossura.auto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.segurossura.auto.model.AssociacaoGrupAssist;
import br.com.segurossura.auto.model.AssociacaoGrupAssistPK;

/**
 * Classe reponsável por obter os dados da tabela de relacionamento entre
 * assistências e grupo corretor.
 * 
 * @author Renan Moreira
 */
@Repository
public interface AssociacaoGrupAssistRepository extends
		CrudRepository<AssociacaoGrupAssist, AssociacaoGrupAssistPK> {

	/**
	 * Responsável por salvar associação entre Grupo Corretor e Assistência.
	 * 
	 * @param grupAssist
	 *            Objeto de associação entre Grupo Corretor e Assistência que
	 *            será salvo.
	 *
	 *
	 */
	@SuppressWarnings("unchecked")
	AssociacaoGrupAssist save(AssociacaoGrupAssist grupAssist);

	@Query(value = "select * " + "from TB_GRUP_CORR_OPCA_PARA "
			+ "where  COD_ORG_PROD = :codOrgProd  "
			+ "AND COD_RAMO = :codRamo " + "AND  COD_PRODUTO = :codProduto "
			+ "AND COD_SUB_RAMO = :codSubRamo " 
			+ "AND COD_PARAM = :codParam "
			+ "AND  COD_OPCAO = :codOpcao "
			+ "and COD_GRUPO_CORR = :codGrupCorr", nativeQuery = true)
	public AssociacaoGrupAssist findById(@Param("codOrgProd") int codOrgProd,
			@Param("codRamo") int codRamo, @Param("codProduto") int codProduto,
			@Param("codSubRamo") int codSubRamo,
			@Param("codOpcao") int codOpcao,
			@Param("codGrupCorr") int codGrupCorr,
			@Param("codParam") int codParam);

	/**
	 * 
	 * Responsável por retornar todas as associações entre Grupo Corretor e
	 * Assistência a partir de um Produto.
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parãmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parâmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parâmetro de busca.
	 * @param codSubRamo
	 *            Código do SubRamo a ser utilizado como parâmetro de busca.
	 * 
	 * @return Retorna todas as associações entre Grupo Corretor e Assistência a
	 *         partir do Órgão Produtor, Ramo, Produto e SubRamo especificados.
	 * 
	 */
	@Query(value = "select TB.COD_EMPR, " 
	 + "TB.COD_OPCAO, "
			+ "TB.COD_ORG_PROD, " + "TB.COD_RAMO,TB.COD_SUB_RAMO, "
			+ "TB.STATUS,TB.COD_GRUPO_CORR, "
			+ "TB.COD_PRODUTO,OP.DESCR_OPCAO, "
			+ "GC.NOME_GRUPO_CORR,P.NOME_PRODUTO "
			+ "  FROM TB_GRUP_CORR_OPCA_PARA TB " + "join VT_PRODUTO P "
			+ "on TB.COD_PRODUTO = P.COD_PRODUTO "
			+ "and TB.COD_RAMO = P.COD_RAMO "
			+ "AND P.COD_ORG_PROD = TB.COD_ORG_PROD "
			+ "JOIN VT_GRUPO_CORR GC "
			+ "ON TB.COD_GRUPO_CORR = GC.COD_GRUPO_CORR "
			+ "JOIN VT_OPCAO_PARAM OP " + "ON TB.COD_OPCAO = OP.COD_OPCAO "
			+ "AND OP.COD_PARAM = :codParam "
			+ " where  TB.COD_ORG_PROD = :codOrgProd  "
			+ "AND TB.COD_RAMO = :codRamo "
			+ "AND TB.COD_SUB_RAMO = :codSubRamo "
			+ "AND P.COD_PRODUTO = :codProduto", nativeQuery = true)
	public List<Object[]> findAllAssociacaoByProduto(
			@Param("codOrgProd") int codOrgProd, @Param("codRamo") int codRamo,
			@Param("codSubRamo") int codSubRamo,
			@Param("codProduto") int codProduto,
			@Param("codParam") int codParam);

	/**
	 * Responsável por retornar todas as associações entre Grupo Corretor e
	 * Assistência a partir de uma Assistência.
	 * 
	 * @param codOpcao
	 *            Código da Opção Parâmetro que representa a Assistência a ser
	 *            utilizada como parâmetro de busca.
	 * 
	 * @return Retorna todas as associações entre Grupo Corretor e Assistência a
	 *         partir da Assistência especificada.
	 * 
	 */
	@Query(value = "select TB.COD_EMPR, " + "TB.COD_OPCAO,TB.COD_ORG_PROD, "
			+ "TB.COD_RAMO,TB.COD_SUB_RAMO, " + "TB.STATUS,TB.COD_GRUPO_CORR, "
			+ "TB.COD_PRODUTO,OP.DESCR_OPCAO, "
			+ "GC.NOME_GRUPO_CORR,P.NOME_PRODUTO, TB.COD_PARAM "
			+ "from TB_GRUP_CORR_OPCA_PARA TB  " + "join VT_PRODUTO P "
			+ "on TB.COD_PRODUTO = P.COD_PRODUTO "
			+ "and TB.COD_RAMO = P.COD_RAMO "
			+ "AND P.COD_ORG_PROD = TB.COD_ORG_PROD "
			+ "JOIN VT_GRUPO_CORR GC "
			+ "ON TB.COD_GRUPO_CORR = GC.COD_GRUPO_CORR "
			+ "JOIN VT_OPCAO_PARAM OP " + "ON TB.COD_OPCAO = OP.COD_OPCAO "
			+ "AND OP.COD_PARAM = TB.COD_PARAM " 
			+ "where  TB.COD_OPCAO = :codOpcao "
			+ "AND TB.COD_PARAM = :codParam ", nativeQuery = true)
	public List<Object[]> findAllAssociacaoByAssistencia(
			@Param("codOpcao") int codOpcao,
			@Param("codParam") int codParam);

	/**
	 * Responsável por retornar todas as associações entre Grupo Corretor e
	 * Assistência a partir de um Grupo corretor.
	 * 
	 * @param codOpcao
	 *            Código da Opção Parâmetro que representa a Assistência a ser
	 *            utilizada como parâmetro de busca.
	 * 
	 * @return Retorna todas as associações entre Grupo Corretor e Assistência a
	 *         partir da Assistência especificada.
	 * 
	 */
	@Query(value = 
			"SELECT "
			+ " TB.COD_EMPR"
			+ ", TB.COD_ORG_PROD"
			+ ", TB.COD_RAMO"
			+ ", TB.COD_SUB_RAMO"
			+ ", TB.COD_PRODUTO"
			+ ", P.NOME_PRODUTO"
			+ ", TB.COD_OPCAO"
			+ ", OP.DESCR_OPCAO"
			+ ", TB.STATUS"
			+ ", TB.COD_GRUPO_CORR"
			+ ", GC.NOME_GRUPO_CORR "
			+ ", TB.COD_PARAM "
			+ "  FROM TB_GRUP_CORR_OPCA_PARA TB  " 
			+ "join VT_PRODUTO P  "
			+ "on TB.COD_PRODUTO = P.COD_PRODUTO  "
			+ "and TB.COD_RAMO = P.COD_RAMO  "
			+ "AND P.COD_ORG_PROD = TB.COD_ORG_PROD  "
			+ "JOIN VT_GRUPO_CORR GC "
			+ "ON TB.COD_GRUPO_CORR = GC.COD_GRUPO_CORR "
			+ "JOIN VT_OPCAO_PARAM OP " + "ON TB.COD_OPCAO = OP.COD_OPCAO "
			+ "AND OP.COD_PARAM = TB.COD_PARAM "
			+ "where  TB.COD_ORG_PROD = :codOrgProd  "
			+ "AND TB.COD_RAMO = :codRamo "
			+ "AND  TB.COD_PRODUTO = :codProduto "
			+ "AND TB.COD_SUB_RAMO = :codSubRamo " 
			+ "AND TB.COD_PARAM = :codParam "
			+ "AND  TB.COD_OPCAO= :codOpcao", nativeQuery = true)
	public List<Object[]> findAllAssociacaoByGrupo(
			@Param("codOrgProd") int codOrgProd, 
			@Param("codRamo") int codRamo,
			@Param("codProduto") int codProduto,
			@Param("codSubRamo") int codSubRamo,
			@Param("codOpcao") int codOpcao,
			@Param("codParam") int codParam);

}
