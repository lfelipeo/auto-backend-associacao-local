package br.com.segurossura.auto.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Classe referente ao conjunto de colunas que representam a Primary Key da
 * tabela de associação entre Grupo Corretor e Assistência.
 * 
 * @author Renan Moreira
 */
@Embeddable
public class AssociacaoGrupAssistPK implements Serializable {

	private static final long serialVersionUID = -6334999293378198040L;

	@Column(name = "COD_EMPR")
	private String codigoEmpr;

	@Column(name = "COD_PARAM")
	private int codigoParam;

	@Column(name = "COD_OPCAO")
	private int codigoOpcao;

	@Column(name = "COD_GRUPO_CORR")
	private int codGrupoCorr;

	@Column(name = "COD_ORG_PROD")
	private int codOrgProd;

	@Column(name = "COD_RAMO")
	private int codRamo;

	@Column(name = "COD_PRODUTO")
	private int codProduto;

	/**
	 * Construtor padrão para instanciação da classe sem passagem de parametros;
	 *
	 */
	public AssociacaoGrupAssistPK() {
	}

	/**
	 * Construtor completo
	 * 
	 * @param codigoEmpr
	 * @param codOrgProd
	 * @param codRamo
	 * @param codProduto
	 * @param codGrupoCorr
	 * @param codigoParam
	 * @param codigoOpcao
	 */
	public AssociacaoGrupAssistPK(String codigoEmpr, int codOrgProd, int codRamo, int codProduto, int codGrupoCorr,
			int codigoParam, int codigoOpcao) {
		this.codigoEmpr = codigoEmpr;
		this.codOrgProd = codOrgProd;
		this.codRamo = codRamo;
		this.codProduto = codProduto;
		this.codGrupoCorr = codGrupoCorr;
		this.codigoParam = codigoParam;
		this.codigoOpcao = codigoOpcao;

	}

	public String getCodigoEmpr() {
		return codigoEmpr;
	}

	public void setCodigoEmpr(String codigoEmpr) {
		this.codigoEmpr = codigoEmpr;
	}

	public int getCodigoParam() {
		return codigoParam;
	}

	public void setCodigoParam(int codigoParam) {
		this.codigoParam = codigoParam;
	}

	public int getCodigoOpcao() {
		return codigoOpcao;
	}

	public void setCodigoOpcao(int codigoOpcao) {
		this.codigoOpcao = codigoOpcao;
	}

	public int getCodGrupoCorr() {
		return codGrupoCorr;
	}

	public void setCodGrupoCorr(int codGrupoCorr) {
		this.codGrupoCorr = codGrupoCorr;
	}

	public int getCodOrgProd() {
		return codOrgProd;
	}

	public void setCodOrgProd(int codOrgProd) {
		this.codOrgProd = codOrgProd;
	}

	public int getCodRamo() {
		return codRamo;
	}

	public void setCodRamo(int codRamo) {
		this.codRamo = codRamo;
	}

	public int getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(int codProduto) {
		this.codProduto = codProduto;
	}

}
