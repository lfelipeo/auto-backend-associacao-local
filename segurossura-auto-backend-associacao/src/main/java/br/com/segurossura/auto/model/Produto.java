package br.com.segurossura.auto.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe referente à tabela de Produto.
 * 
 * @author Renan Moreira
 */
@Entity
@Table(name="VT_PRODUTO")
public class Produto {
	
	@EmbeddedId
	private ProdutoPK id;
	
	@Column(name="COD_SUB_RAMO")
	private int codSubRamo;
	
	@Column(name="NOME_PRODUTO")
	private String nomeProduto;

	public ProdutoPK getId() {
		return id;
	}

	public void setId(ProdutoPK id) {
		this.id = id;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public int getCodSubRamo() {
		return codSubRamo;
	}

	public void setCodSubRamo(int codSubRamo) {
		this.codSubRamo = codSubRamo;
	}

}
