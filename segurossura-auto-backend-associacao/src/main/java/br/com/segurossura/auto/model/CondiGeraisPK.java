package br.com.segurossura.auto.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Classe referente ao conjunto de colunas que representam a Primary Key da
 * tabela de Condicoes Gerais.
 * 
 * @author Renan Moreira
 * @since 26/10/2018
 */
@Embeddable
public class CondiGeraisPK implements Serializable {

	private static final long serialVersionUID = -3676207504982348745L;

	/**
	 * Código Empersa
	 */

	@Column(name = "COD_EMPR")
	private String codigoEmpr;

	/**
	 * Codigo Report
	 */

	@Column(name = "COD_REPORT")
	private String codReport;

	/**
	 * Código Ramo
	 */

	@Column(name = "COD_RAMO")
	private int codRamo;

	private int codSubRamo;

	/**
	 * Dt Inicio Versão
	 */

	@Column(name = "DT_INIC_VERSAO")
	private String dtInicVersao;

	public String getCodigoEmpr() {
		return codigoEmpr;
	}

	public void setCodigoEmpr(String codigoEmpr) {
		this.codigoEmpr = codigoEmpr;
	}

	public int getCodRamo() {
		return codRamo;
	}

	public void setCodRamo(int codRamo) {
		this.codRamo = codRamo;
	}

	public String getCodReport() {
		return codReport;
	}

	public void setCodReport(String codReport) {
		this.codReport = codReport;
	}

	public String getDtInicVersao() {
		return dtInicVersao;
	}

	public void setDtInicVersao(String dtInicVersao) {
		this.dtInicVersao = dtInicVersao;
	}

	public int getCodSubRamo() {
		return codSubRamo;
	}

	public void setCodSubRamo(int codSubRamo) {
		this.codSubRamo = codSubRamo;
	}

}
