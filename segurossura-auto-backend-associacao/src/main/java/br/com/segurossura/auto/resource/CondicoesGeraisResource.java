package br.com.segurossura.auto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.segurossura.auto.model.CondiGerais;
import br.com.segurossura.auto.repository.CondiGeraisRepository;

@RestController
@RequestMapping({ "/condicoes" })
public class CondicoesGeraisResource {
	
	@Autowired
	private CondiGeraisRepository repository;

	/**
	 * 
	 * Método responsável por retornar todas as Condições Gerais a partir de um
	 * Produto.
	 * 
	 * @param codProduto
	 *            Código do Produto a ser utilizado na busca.
	 * @return Retorna uma lista de todos as Condições Gerais a partir do
	 *         produto especificado.
	 * 
	 */
	@GetMapping
	public List<CondiGerais> findAllConditions(int codProduto) {
		return repository.findAllConditions(codProduto);
	}

}
