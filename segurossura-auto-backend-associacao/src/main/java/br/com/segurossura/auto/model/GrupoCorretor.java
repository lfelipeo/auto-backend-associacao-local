package br.com.segurossura.auto.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe referente à tabela de Grupo Corretor.
 * 
 * @author Renan Moreira
 */
@Entity
@Table(name = "VT_GRUPO_CORR")
public class GrupoCorretor {

	@EmbeddedId
	private GrupoCorretorPK id;

	@Column(name = "NOME_GRUPO_CORR")
	private String nomeGrupoCorr;

	public GrupoCorretorPK getId() {
		return id;
	}

	public void setId(GrupoCorretorPK id) {
		this.id = id;
	}

	public String getNomeGrupoCorr() {
		return nomeGrupoCorr;
	}

	public void setNomeGrupoCorr(String nomeGrupoCorr) {
		this.nomeGrupoCorr = nomeGrupoCorr;
	}

}
