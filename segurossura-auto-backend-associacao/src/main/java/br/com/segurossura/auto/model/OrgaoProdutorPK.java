package br.com.segurossura.auto.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Classe referente ao conjunto de colunas que representam a Primary Key da
 * tabela de Órgão Produtor..
 * 
 * @author Renan Moreira
 */
@Embeddable
public class OrgaoProdutorPK implements Serializable {

	private static final long serialVersionUID = -2032642351786242229L;

	@Column(name = "COD_ORG_PROD")
	private int codOrgProd;

	@Column(name = "COD_EMPR")
	private String codigoEmpr;

	public int getCodOrgProd() {
		return codOrgProd;
	}

	public void setCodOrgProd(int codOrgProd) {
		this.codOrgProd = codOrgProd;
	}

	public String getCodigoEmpr() {
		return codigoEmpr;
	}

	public void setCodigoEmpr(String codigoEmpr) {
		this.codigoEmpr = codigoEmpr;
	}

}
