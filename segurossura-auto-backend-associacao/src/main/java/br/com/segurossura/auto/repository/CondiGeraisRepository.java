package br.com.segurossura.auto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.segurossura.auto.model.CondiGerais;
import br.com.segurossura.auto.model.CondiGeraisPK;

@Repository
public interface CondiGeraisRepository extends CrudRepository<CondiGerais, CondiGeraisPK> {

	/**
	 * Responsável por retornar todas as Condições Gerais a partir de um
	 * Produto.
	 * 
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parâmetro de busca.
	 * @return Retorna todas as Condições Gerais com o Produto especificado.
	 */

	@Query(value = "SELECT DISTINCT rce.* 							  "
			+ "  FROM vt_relat_cond_empr rce,                         "
			+ "       vt_prod_cond_geral pcg,                         "
			+ "       vt_produto         prd                          "
			+ " where prd.cod_empr           = 'G'                    "
			+ "   and prd.cod_ramo           = 310                    "
			+ "   and pcg.cod_produto        = :codProduto			  "
			+ "   AND pcg.cod_empr           = prd.cod_empr           "
			+ "   AND pcg.cod_org_prod       = prd.cod_org_prod_pai   "
			+ "   AND pcg.cod_ramo           = prd.cod_ramo           "
			+ "   AND pcg.cod_produto        = prd.cod_produto_pai    "
			+ "   AND rce.cod_empr           = pcg.cod_empr           "
			+ "   AND rce.cod_report         = pcg.cod_report         "
			+ "   AND rce.dt_inic_versao     = pcg.dt_inic_versao     "
			+ "   AND PCG.DESL_S_N           = 'N'                    "
			+ "   AND PCG.COND_GERAL_COB_S_N = 'S'					  "
			+ "  order by rce.nome_condicao ", nativeQuery = true)
	public List<CondiGerais> findAllConditions(@Param("codProduto") int codProduto);
}
