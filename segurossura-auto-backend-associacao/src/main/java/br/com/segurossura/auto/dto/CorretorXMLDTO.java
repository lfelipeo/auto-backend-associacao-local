package br.com.segurossura.auto.dto;

import java.io.Serializable;

/**
 * Classe DTO que recebe o corretor enviado através do Servlet.
 * 
 * @author Renan Moreira
 */
public class CorretorXMLDTO implements Serializable {

	private static final long serialVersionUID = 6232431256691921265L;

	private String codOrgProd;
	private String codCorretor;
	private String codCorretorSusep;
	private String nomeCorretor;
	
	public String getCodOrgProd() {
		return codOrgProd;
	}
	
	public String getCodCorretor() {
		return codCorretor;
	}
	
	public String getCodCorretorSusep() {
		return codCorretorSusep;
	}
	
	public String getNomeCorretor() {
		return nomeCorretor;
	}
	
}
