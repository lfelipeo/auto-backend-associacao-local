package br.com.segurossura.auto.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe referente à entidade SubRamo.
 * 
 * @author Renan Moreira
 */
@Entity
@Table(name = "VT_SUB_RAMO")
public class SubRamo {

	@EmbeddedId
	private SubRamoPK id;

	@Column(name = "NOME_SUB_RAMO")
	private String nomeSubRamo;

	public SubRamoPK getId() {
		return id;
	}

	public void setId(SubRamoPK id) {
		this.id = id;
	}

	public String getNomeSubRamo() {
		return nomeSubRamo;
	}

	public void setNomeSubRamo(String nomeSubRamo) {
		this.nomeSubRamo = nomeSubRamo;
	}

}
