package br.com.segurossura.auto.security;

import static java.util.Base64.getMimeDecoder;
import static javax.crypto.Cipher.DECRYPT_MODE;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class DescriptografiaLoginXml {

	private static final String CHAVE_CRIPTOGRAFIA_LOGIN = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI3YWIyODQ4Yy00ZGFmLTRkOGQtYmFjNC1hNTBhY2JiNmQ3N2EiLCJzdWIiOiI0MjM1IiwidGlwb0Z1bmNpb25hcmlvIjoiIiwibm9tZVVzdWFyaW8iOiJMdXphcmRvIHNlZ3Vyb3MiLCJ0aXBvVXN1YXJpbyI6IkMiLCJpYXQiOjE0OTY2ODg1ODQsImV4cCI6MzYwMDE0OTY2ODQ5ODR9.h2phI9oWOaytlmAWCp9rO9KNKSceXvK2q2RhdEbneoeyHqjZpGFdHV1DcU4BwcyKGuUpg6DUXmpRfOxiqrXifQ";

	private DescriptografiaLoginXml() {
	}

	/**
	 * 
	 * Método responsável por descriptografar um Login em String
	 * 
	 * @param cryptedFile
	 *            Arquvo encriptado
	 * @param key
	 *            Chave de autenticação
	 * 
	 */
	public static String loginDescrypt(String cryptedFile, String key) throws Exception {
		return DescriptografiaLoginXml.decrypt(cryptedFile, key);
	}

	/**
	 * 
	 * Método responsável por descriptografar um Login em String
	 * 
	 * @param cryptedFile
	 *            Arquvo encriptado
	 * 
	 */
	public static String loginDescrypt(String cryptedFile) throws Exception {
		return DescriptografiaLoginXml.decrypt(cryptedFile, CHAVE_CRIPTOGRAFIA_LOGIN);
	}

	/**
	 * 
	 * Método responsável por descriptografar um Login em String
	 * 
	 * @param cryptedFile
	 *            Arquvo encriptado
	 * @param key
	 *            Chave de autenticação
	 * 
	 */

	private static String decrypt(String cryptedFile, String key) throws Exception {
		final byte[] encryptKey = getMimeDecoder().decode(key);
		final DESKeySpec desKeySpec = new DESKeySpec(encryptKey);
		final SecretKey secretKey = SecretKeyFactory.getInstance("DES").generateSecret(desKeySpec);
		final Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(DECRYPT_MODE, secretKey);

		return new String(cipher.doFinal(getMimeDecoder().decode(cryptedFile)));
	}

}
