package br.com.segurossura.auto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import br.com.segurossura.auto.model.Produto;
import br.com.segurossura.auto.model.ProdutoPK;

public interface ProdutoRepository extends CrudRepository<Produto, ProdutoPK> {

	/**
	
	Responsável por retornar todos os Produtos a partir de um Órgão Produtor, Ramo e SubRamo.

	@param codOrgProd  Código do Órgão Produtor a ser utilizado como parãmetro de busca.
	@param codRamo     Código do Ramo a ser utilizado como parãmetro de busca.
	@param codSubRamo  Código do SubRamo a ser utilizado como parãmetro de busca.
	
	@return  Retorna todos os Produtos a partir do Órgão Produtor, Ramo e SubRamo especificados.
	
	*/	
	
	@Query("select p from Produto p "
			+ "where p.id.codOrgProd = :codOrgProd "
			+ "AND p.id.codRamo = :codRamo "
			+ "and p.codSubRamo = :codSubRamo "
			+ "ORDER BY p.nomeProduto ASC")
	public List<Produto> findAllProduto(@Param("codOrgProd") int codOrgProd,@Param("codRamo") int codRamo,@Param("codSubRamo") int codSubRamo);

}
