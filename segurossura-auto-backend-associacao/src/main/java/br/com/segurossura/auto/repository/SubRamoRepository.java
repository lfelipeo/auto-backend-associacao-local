package br.com.segurossura.auto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.segurossura.auto.model.SubRamo;
import br.com.segurossura.auto.model.SubRamoPK;

public interface SubRamoRepository extends CrudRepository<SubRamo, SubRamoPK> {

	/**
	
	Método responsável por retornar todos os SubRamos.
	
	@param codRamo Código do ramo a ser utilizado como parâmetro de busca.
	
	@return        Retorna todas os SubRamos.
	
	*/

	@Query(value="SELECT * "
			+ "FROM VT_SUB_RAMO "
			+ "WHERE COD_EMPR ='G' "
			+ "AND DESL_S_N = 'N' "
			+ "AND COD_RAMO = :codRamo "
			+ "ORDER BY NOME_SUB_RAMO  ASC", nativeQuery=true)
	public List<SubRamo> findAllSubRamo(@Param("codRamo") int codRamo);
}
