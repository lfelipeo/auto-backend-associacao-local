package br.com.segurossura.auto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import br.com.segurossura.auto.model.GrupoCorretor;
import br.com.segurossura.auto.model.GrupoCorretorPK;

@Repository
public interface GrupoCorretorRepository extends CrudRepository<GrupoCorretor, GrupoCorretorPK> {

	/**
	 * 
	 * Responsável por retornar todos os Grupos Corretores que forem do tipo de Grupo 3.
	 * 
	 * @return Retorna todos os Grupos Corretores.
	 * 
	 */

	@Query(value = 
			"SELECT * FROM SQLDBA.VT_GRUPO_CORR GC "
			+ "WHERE GC.DESL_S_N = 'N'"
			+ " AND GC.TIPO_GRUPO = 3"
			+ " ORDER BY NOME_GRUPO_CORR ASC", nativeQuery = true)
	public List<GrupoCorretor> findAllGrupoCorretor();
}
