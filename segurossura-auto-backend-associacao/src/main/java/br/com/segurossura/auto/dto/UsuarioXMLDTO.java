package br.com.segurossura.auto.dto;

import java.io.Serializable;

/**
 * Classe DTO que recebe o usuário logado enviado através do Servlet.
 * 
 * @author Renan Moreira
 */
public class UsuarioXMLDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String tipoCanal;
	private String tipoUsuario;
	private String tipoFuncionario;
	private Integer matrUsu;
	private Integer idUsuWeb;
	private String nomeUsuario;
	private String validade;
	private String codOrgPro;
	private String codCentralCorretor;
	private String perfil;
	private String loginWeb;
	private String codGrupoCorretor;
	private String codCentralCorretorCotacao;
	private String numeroSPE;
	private String codigoEmpresa;

	public String getTipoCanal() {
		return tipoCanal;
	}

	public void setTipoCanal(String tipoCanal) {
		this.tipoCanal = tipoCanal;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getTipoFuncionario() {
		return tipoFuncionario;
	}

	public void setTipoFuncionario(String tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}

	public Integer getMatrUsu() {
		return matrUsu;
	}

	public void setMatrUsu(Integer matrUsu) {
		this.matrUsu = matrUsu;
	}

	public Integer getIdUsuWeb() {
		return idUsuWeb;
	}

	public void setIdUsuWeb(Integer idUsuWeb) {
		this.idUsuWeb = idUsuWeb;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getValidade() {
		return validade;
	}

	public void setValidade(String validade) {
		this.validade = validade;
	}

	public String getCodOrgPro() {
		return codOrgPro;
	}

	public void setCodOrgPro(String codOrgPro) {
		this.codOrgPro = codOrgPro;
	}

	public String getCodCentralCorretor() {
		return codCentralCorretor;
	}

	public void setCodCentralCorretor(String codCentralCorretor) {
		this.codCentralCorretor = codCentralCorretor;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getLoginWeb() {
		return loginWeb;
	}

	public void setLoginWeb(String loginWeb) {
		this.loginWeb = loginWeb;
	}

	public String getCodGrupoCorretor() {
		return codGrupoCorretor;
	}

	public void setCodGrupoCorretor(String codGrupoCorretor) {
		this.codGrupoCorretor = codGrupoCorretor;
	}

	public String getCodCentralCorretorCotacao() {
		return codCentralCorretorCotacao;
	}

	public void setCodCentralCorretorCotacao(String codCentralCorretorCotacao) {
		this.codCentralCorretorCotacao = codCentralCorretorCotacao;
	}
	
	public String getCodigoEmpresa() {
		return codigoEmpresa;
	}
	
	public void setCodigoEmpresa(String codigoEmpresa) {
		this.codigoEmpresa = codigoEmpresa;
	}
	
	public String getNumeroSPE() {
		return numeroSPE;
	}
	
	public void setNumeroSPE(String numeroSPE) {
		this.numeroSPE = numeroSPE;
	}

}
