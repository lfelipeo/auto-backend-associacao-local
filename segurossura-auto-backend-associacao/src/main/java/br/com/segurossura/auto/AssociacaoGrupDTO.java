package br.com.segurossura.auto;

/**
 * Classe DTO que representa um objeto de associação com Grupo Corretor.
 * 
 * @author Renan Moreira
 */
public class AssociacaoGrupDTO extends AssociacaoDTO {

	private int codGrupoCorr;

	private int codSubRamo;

	public int getCodGrupoCorr() {
		return codGrupoCorr;
	}

	public void setCodGrupoCorr(int codGrupoCorr) {
		this.codGrupoCorr = codGrupoCorr;
	}

	public int getCodSubRamo() {
		return codSubRamo;
	}

	public void setCodSubRamo(int codSubRamo) {
		this.codSubRamo = codSubRamo;
	}

}
