package br.com.segurossura.auto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.segurossura.auto.model.CodParam;
import br.com.segurossura.auto.repository.CodParamRepository;

@RestController
@CrossOrigin
@RequestMapping({ "/assistencia" })
public class AssistanceResource {

	@Autowired
	private CodParamRepository repository;

	/**
	 * Método responsável por retornar todas as Assistências.
	 * 
	 * @return Retorna todas as Assistências.
	 */

	@GetMapping
	public List<CodParam> findAllAssistance() {
		return repository.findAllAssistance();
	}

}
