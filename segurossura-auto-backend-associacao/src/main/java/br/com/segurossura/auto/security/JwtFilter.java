package br.com.segurossura.auto.security;

import static java.util.Objects.isNull;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.JwtException;

	/**
	* Classe para configuração do JSON Web Token;
	* @see JSON Web Token	
	*/
@Component
public class JwtFilter implements Filter {

	private final Logger logger = LoggerFactory.getLogger(JwtFilter.class);

	@Autowired
	private JwtService jwtTokenService;

	/**
	 * 
	 * Insere o usuário na resposta e requisição recebida pelo Servlet para
	 * autenticação.
	 * 
	 * @param request
	 *            Requisição recebida via Servlet.
	 * @param response
	 *            Resposta recebida via Servlet.
	 * 
	 */

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final HttpServletResponse httpResponse = (HttpServletResponse) response;
		final String authHeaderVal = httpRequest.getHeader(AUTHORIZATION);

		if (isNull(authHeaderVal)) {
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}

		final String token = authHeaderVal.substring("Bearer".length(), authHeaderVal.length());

		try {
			httpRequest.setAttribute("jwtUser", jwtTokenService.getUser(token));
			chain.doFilter(httpRequest, httpResponse);
		} catch (JwtException e) {
			logger.error("[JwtFilter.doFilter]", e);
			httpResponse.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
		}
	}

	/**
	 * 
	 * Loga informação de destruir sessão do Filter.
	 * 
	 */

	@Override
	public void destroy() {
		logger.info("[JwtFilter.destroy]");
	}

	/**
	 * 
	 * Loga informação de iniciar sessão do Filter.
	 * 
	 * @param arg0
	 *            Configuração do Filter
	 * 
	 */

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		logger.info("[JwtFilter.init]");
	}

}
