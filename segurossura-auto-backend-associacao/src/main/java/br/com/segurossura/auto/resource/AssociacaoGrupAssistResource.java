package br.com.segurossura.auto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.segurossura.auto.AssociacaoGrupDTO;
import br.com.segurossura.auto.dto.GrupoCorretorAssistenciaDTO;
import br.com.segurossura.auto.model.AssociacaoGrupAssist;
import br.com.segurossura.auto.model.AssociacaoGrupAssistPK;
import br.com.segurossura.auto.repository.AssociacaoGrupAssistRepository;
import br.com.segurossura.auto.service.GrupoCorretorAssistenciaService;
import br.com.segurossura.auto.util.ConstantesUtil;

@RestController
@RequestMapping({ "/associacaoGrupAssist" })
public class AssociacaoGrupAssistResource {

	@Autowired
	private AssociacaoGrupAssistRepository repository;

	@Autowired
	private GrupoCorretorAssistenciaService service;

	
	/**
	 * 
	 * Método responsável por salvar a associação entre Grupo Corretor e
	 * Assistência.
	 * 
	 * @param associaDTO
	 *            Objeto DTO que recebe as informações da Request, inclui em
	 *            seus atributos e é utilizado para gerar o objeto de
	 *            associação.
	 * 
	 * @return Retorna o status Created após salvar o objeto de associação no
	 *         Banco de Dados.
	 * 
	 */

	@PostMapping({ "/salvar" })
	public ResponseEntity<Object> saveAssociacao(@RequestBody AssociacaoGrupDTO associaDTO) {

		AssociacaoGrupAssist associa = new AssociacaoGrupAssist();
		
		associa.setId(new AssociacaoGrupAssistPK(
				associaDTO.getCodigoEmpr(),
				associaDTO.getCodOrgProd(),
				associaDTO.getCodRamo(),
				associaDTO.getCodProduto(),
				associaDTO.getCodGrupoCorr(),
				associaDTO.getCodigoParam(),
				associaDTO.getCodigoOpcao()));
		associa.setCodSubRamo(associaDTO.getCodSubRamo());
		associa.setStatus("S");
		
		repository.save(associa);
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	/**
	 * 
	 * Método responsável por buscar Produtos apartir de um Grupo
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parãmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parãmetro de busca.
	 * @param codSubRamo
	 *            Código do Ramo a ser utilizado como parãmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parãmetro de busca.
	 * 
	 */

	@GetMapping({ "/consultasProdutoByGrup" })
	public List<GrupoCorretorAssistenciaDTO> findAllAssociacaoByProduto(int codOrgProd, int codRamo, int codSubRamo,
			int codProduto, int codParam) {
		return service.findAllAssociacaoByProduto(codOrgProd, codRamo, codSubRamo, codProduto, codParam);
	}

	@GetMapping({ "/consultasAssistenciaByGrup" })
	public List<GrupoCorretorAssistenciaDTO> findAllAssociacaoByAssistencia(int codOpcao, int codParam) {

		return service.findAllAssociacaoByAssistencia(codOpcao, codParam);

	}

	/**
	 * 
	 * Método responsável por retornar todas as associações entre um Grupo
	 * Corretor e Assistência a partir de um Grupo Corretor.
	 * 
	 * @param codOpcao
	 *            Código da Opção Parâmetro que representa uma Assistência a ser
	 *            utilizada como parãmetro de busca.
	 * 
	 * @return Retorna todas as associações entre um Grupo Corretor e
	 *         Assistência a partir do Grupo Corretor especificado.
	 * 
	 */

	@GetMapping({ "/consultasAssociacaoByGrup" })
	public List<GrupoCorretorAssistenciaDTO> findAllAssociacaoByGrupo(
			int codOrgProd, int codRamo, int codProduto, int codSubRamo, int codOpcao, int codParam) {
		return service.findAllAssociacaoByGrupo(codOrgProd, codRamo, codProduto, codSubRamo, codOpcao, codParam);
	}

	/**
	 * 
	 * Método responsável pela alteração dos estados das Associações;
	 * 
	 * @param AssistenciaCondicoesGeraisDTO
	 *            recebe a associação para ser buscada no banco;
	 * 
	 * @return Retorna a Associação alterada;
	 * 
	 */
	@PutMapping({ "/alterar" })
	public @ResponseBody AssociacaoGrupAssist updateNote(
			@RequestParam Integer codRamo, 
			@RequestParam Integer codProduto,
			@RequestParam("codSubRamo") Integer codSubRamo, 
			@RequestParam("codOpcao") Integer codOpcao,
			@RequestParam("codGrupCorr") Integer codGrupCorr,
			@RequestBody AssociacaoGrupAssist assist,
			@RequestParam("codParam") Integer codParam) {

		// Foi chumbado o número 1 no Orgão Produtor pois foi acordado
		// que sempre será Matriz (1). 05/12/2018
		// Luiz Oliveira.
		
		AssociacaoGrupAssist associao = repository.findById(ConstantesUtil.NUMERO_UM, codRamo, codProduto, codSubRamo, codOpcao,
				codGrupCorr, codParam);

		if ("N".equals(associao.getStatus())) {
			associao.setStatus("S");
		} else {
			associao.setStatus("N");
		}

		return repository.save(associao);

	}
}
