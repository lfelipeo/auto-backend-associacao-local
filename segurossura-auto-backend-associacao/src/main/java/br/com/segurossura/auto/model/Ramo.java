package br.com.segurossura.auto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe referente à entidade Ramo.
 * 
 * @author Renan Moreira
 */
@Entity
@Table(name = "VT_RAMO")
public class Ramo {
	@Id
	@Column(name = "COD_RAMO")
	private Integer codRamo;

	@Column(name = "NOME_RAMO")
	private String nomeRamo;

	public int getCodRamo() {
		return codRamo;
	}

	public void setCodRamo(int codRamo) {
		this.codRamo = codRamo;
	}

	public String getNomeRamo() {
		return nomeRamo;
	}

	public void setNomeRamo(String nomeRamo) {
		this.nomeRamo = nomeRamo;
	}
}
