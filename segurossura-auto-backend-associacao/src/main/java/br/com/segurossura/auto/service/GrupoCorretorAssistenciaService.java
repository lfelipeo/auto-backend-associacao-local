package br.com.segurossura.auto.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.segurossura.auto.dto.GrupoCorretorAssistenciaDTO;
import br.com.segurossura.auto.repository.AssociacaoGrupAssistRepository;
import br.com.segurossura.auto.util.ConstantesUtil;

@Service
public class GrupoCorretorAssistenciaService {

	@Autowired
	private AssociacaoGrupAssistRepository repository;

	/**
	 * GrupoCorretorAssistenciaDTO
	 * 
	 * @param collection
	 * @return retorna uma list GrupoCorretorAssistenciaDTO
	 */

	public List<GrupoCorretorAssistenciaDTO> convertCollection(List<Object[]> collection) {

		List<GrupoCorretorAssistenciaDTO> list = new ArrayList<>();

		for (Object[] obj : collection) {

			GrupoCorretorAssistenciaDTO dto = new GrupoCorretorAssistenciaDTO();

			dto.setCodigoEmpr(String.valueOf(obj[ConstantesUtil.NUMERO_ZERO]));
			dto.setCodOrgProd(String.valueOf(obj[ConstantesUtil.NUMERO_UM]));
			dto.setCodRamo(String.valueOf(obj[ConstantesUtil.NUMERO_DOIS]));
			dto.setCodSubRamo(String.valueOf(obj[ConstantesUtil.NUMERO_TRES]));
			dto.setCodProduto(String.valueOf(obj[ConstantesUtil.NUMERO_QUATRO]));
			dto.setNomeProduto(String.valueOf(obj[ConstantesUtil.NUMERO_CINCO]));
			dto.setCodigoOpcao(String.valueOf(obj[ConstantesUtil.NUMERO_SEIS]));
			dto.setDescrOpcao(String.valueOf(obj[ConstantesUtil.NUMERO_SETE]));
			dto.setStatus(String.valueOf(obj[ConstantesUtil.NUMERO_OITO]));
			dto.setCodGrupoCorr(String.valueOf(obj[ConstantesUtil.NUMERO_NOVE]));
			dto.setNomeGrupoCorr(String.valueOf(obj[ConstantesUtil.NUMERO_DEZ]));
			dto.setCodParam(String.valueOf(obj[ConstantesUtil.NUMERO_ONZE]));

			list.add(dto);
		}
		return list;
	}

	/**
	 * 
	 * findAllAssociacaoByAssistencia
	 * 
	 * @param codOpcao
	 * @return retorna List<GrupoCorretorAssistenciaDTO>
	 */

	public List<GrupoCorretorAssistenciaDTO> findAllAssociacaoByAssistencia(int codOpcao, int codParam) {

		List<Object[]> list;

		list = repository.findAllAssociacaoByAssistencia(codOpcao, codParam);

		return convertCollection(list);

	}

	/**
	 * 
	 * findAllAssociacaoByGrupo
	 * 
	 * @param codOrgProd
	 * @param codRamo
	 * @param codProduto
	 * @param codSubRamo
	 * @param codOpcao
	 * @return retorna List<GrupoCorretorAssistenciaDTO>
	 */

	public List<GrupoCorretorAssistenciaDTO> findAllAssociacaoByGrupo(int codOrgProd, int codRamo, int codProduto,
			int codSubRamo, int codOpcao, int codParam) {

		List<Object[]> list;

		list = repository.findAllAssociacaoByGrupo(codOrgProd, codRamo, codProduto, codSubRamo, codOpcao, codParam);

		return convertCollection(list);
	}

	/**
	 * 
	 * findAllAssociacaoByProduto
	 * 
	 * @param codOrgProd
	 * @param codRamo
	 * @param codSubRamo
	 * @param codProduto
	 * @return List<GrupoCorretorAssistenciaDTO>
	 */

	public List<GrupoCorretorAssistenciaDTO> findAllAssociacaoByProduto(int codOrgProd, int codRamo, int codSubRamo,
			int codProduto, int codParam) {
		List<Object[]> list;

		list = repository.findAllAssociacaoByProduto(codOrgProd, codRamo, codSubRamo, codProduto, codParam);

		return convertCollection(list);

	}

}
