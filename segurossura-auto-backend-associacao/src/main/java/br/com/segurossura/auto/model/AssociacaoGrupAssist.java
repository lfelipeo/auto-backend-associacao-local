package br.com.segurossura.auto.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Classe referente à tabela de associação entre Grupo Corretor e Assistência
 * (Opção Parâmetro).
 * 
 * @author Renan Moreira
 * @since 26/10/2018
 */
@Entity
@Table(name = "TB_GRUP_CORR_OPCA_PARA")
public class AssociacaoGrupAssist {

	@EmbeddedId
	private AssociacaoGrupAssistPK id;

	@Column(name = "COD_SUB_RAMO")
	private int codSubRamo;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "DT_INCL")
	private String dtIncl;

	@Column(name = "USU_INCL")
	private Integer usuIncl;

	@Column(name = "DT_ALT")
	private String dtAlt;

	@Column(name = "HORA_ALT")
	private String horaAlt;

	@Column(name = "USU_ALT")
	private Integer usuAlt;

	public int getCodSubRamo() {
		return codSubRamo;
	}

	public void setCodSubRamo(int codSubRamo) {
		this.codSubRamo = codSubRamo;
	}

	public AssociacaoGrupAssistPK getId() {
		return id;
	}

	public void setId(AssociacaoGrupAssistPK id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDtIncl() {
		return dtIncl;
	}

	public void setDtIncl(String dtIncl) {
		this.dtIncl = dtIncl;
	}

	public Integer getUsuIncl() {
		return usuIncl;
	}

	public void setUsuIncl(Integer usuIncl) {
		this.usuIncl = usuIncl;
	}

	public String getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(String dtAlt) {
		this.dtAlt = dtAlt;
	}

	public String getHoraAlt() {
		return horaAlt;
	}

	public void setHoraAlt(String horaAlt) {
		this.horaAlt = horaAlt;
	}

	public Integer getUsuAlt() {
		return usuAlt;
	}

	public void setUsuAlt(Integer usuAlt) {
		this.usuAlt = usuAlt;
	}

}
