package br.com.segurossura.auto.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Classe referente ao conjunto de colunas que representam a Primary Key da
 * tabela de SubRamo.
 * 
 * @author Renan Moreira
 */
@Embeddable
public class SubRamoPK implements Serializable {

	private static final long serialVersionUID = -4642920992925616322L;

	@Column(name = "COD_EMPR")
	private String codigoEmpr;

	@Column(name = "COD_SUB_RAMO")
	private int codSubRamo;

	@Column(name = "COD_RAMO")
	private int codRamo;

	public String getCodigoEmpr() {
		return codigoEmpr;
	}

	public void setCodigoEmpr(String codigoEmpr) {
		this.codigoEmpr = codigoEmpr;
	}

	public int getCodSubRamo() {
		return codSubRamo;
	}

	public void setCodSubRamo(int codSubRamo) {
		this.codSubRamo = codSubRamo;
	}

	public int getCodRamo() {
		return codRamo;
	}

	public void setCodRamo(int codRamo) {
		this.codRamo = codRamo;
	}

}
