package br.com.segurossura.auto.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Classe referente ao conjunto de colunas que representam a Primary Key da
 * tabela de associação entre Condição Geral e Assistência.
 * 
 * @author Renan Moreira
 */
@Embeddable
public class AssociacaoAssistCondiPK implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Código Empresa
	 */

	@Column(name = "COD_EMPR")
	private String codigoEmpr;

	/**
	 * Código Param.
	 */

	@Column(name = "COD_PARAM")
	private int codigoParam;

	/**
	 * Código Opção
	 */

	@Column(name = "COD_OPCAO")
	private int codigoOpcao;

	/**
	 * Código Prod.
	 */

	@Column(name = "COD_ORG_PROD")
	private int codOrgProd;

	/**
	 * Código Ramo
	 */

	@Column(name = "COD_RAMO")
	private int codRamo;

	/**
	 * Código Produto
	 */

	@Column(name = "COD_PRODUTO")
	private int codProduto;

	@Column(name = "COD_REPORT")
	private String codReport;

	@Column(name = "ORDEM_IMPR")
	private int ordemImpr;

	public AssociacaoAssistCondiPK() {
	}

	public AssociacaoAssistCondiPK(int codigoOpcao, int codOrgProd, int codRamo, int codProduto, String codReport,
			int ordemImpr, int codParam) {
		this.codigoOpcao = codigoOpcao;
		this.codigoParam = codParam;
		this.codOrgProd = codOrgProd;
		this.codProduto = codProduto;
		this.codigoEmpr = "G";
		this.codRamo = codRamo;
		this.codReport = codReport;
		this.ordemImpr = ordemImpr;
	}

	public String getCodigoEmpr() {
		return codigoEmpr;
	}

	public void setCodigoEmpr(String codigoEmpr) {
		this.codigoEmpr = codigoEmpr;
	}

	public int getCodigoParam() {
		return codigoParam;
	}

	public void setCodigoParam(int codigoParam) {
		this.codigoParam = codigoParam;
	}

	public int getCodigoOpcao() {
		return codigoOpcao;
	}

	public void setCodigoOpcao(int codigoOpcao) {
		this.codigoOpcao = codigoOpcao;
	}

	public int getCodOrgProd() {
		return codOrgProd;
	}

	public void setCodOrgProd(int codOrgProd) {
		this.codOrgProd = codOrgProd;
	}

	public int getCodRamo() {
		return codRamo;
	}

	public void setCodRamo(int codRamo) {
		this.codRamo = codRamo;
	}

	public int getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(int codProduto) {
		this.codProduto = codProduto;
	}

	public String getCodReport() {
		return codReport;
	}

	public void setCodReport(String codReport) {
		this.codReport = codReport;
	}


	public int getOrdemImpr() {
		return ordemImpr;
	}

	public void setOrdemImpr(int ordemImpr) {
		this.ordemImpr = ordemImpr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codOrgProd;
		result = prime * result + codProduto;
		result = prime * result + codRamo;
		result = prime * result + ((codReport == null) ? 0 : codReport.hashCode());
		result = prime * result + ((codigoEmpr == null) ? 0 : codigoEmpr.hashCode());
		result = prime * result + codigoOpcao;
		result = prime * result + codigoParam;
		result = prime * result + ordemImpr;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean returnValue = true;

		if (this == obj)
			returnValue = true;
		if (obj == null)
			returnValue = false;
		if (getClass() != obj.getClass())
			returnValue = false;
		AssociacaoAssistCondiPK other = (AssociacaoAssistCondiPK) obj;
		if (codOrgProd != other.codOrgProd)
			returnValue = false;
		if (codProduto != other.codProduto)
			returnValue = false;
		if (codRamo != other.codRamo)
			returnValue = false;
		if (codReport == null) {
			if (other.codReport != null)
				returnValue = false;
		} else if (!codReport.equals(other.codReport))
			returnValue = false;
		if (codigoEmpr == null) {
			if (other.codigoEmpr != null)
				returnValue = false;
		} else if (!codigoEmpr.equals(other.codigoEmpr))
			returnValue = false;
		if (codigoOpcao != other.codigoOpcao)
			returnValue = false;
		if (codigoParam != other.codigoParam)
			returnValue = false;
		if (ordemImpr != other.ordemImpr)
			returnValue = false;

		return returnValue;
	}

}
