package br.com.segurossura.auto.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Classe referente ao conjunto de colunas que representam a Primary Key da
 * tabela de Produto.
 * 
 * @author Renan Moreira
 */
@Embeddable
public class ProdutoPK implements Serializable {

	private static final long serialVersionUID = -1505804204467099397L;

	@Column(name = "COD_EMPR")
	private String codigoEmpr;

	@Column(name = "COD_ORG_PROD")
	private int codOrgProd;

	@Column(name = "COD_RAMO")
	private int codRamo;

	@Column(name = "COD_PRODUTO")
	private int codProduto;

	public String getCodigoEmpr() {
		return codigoEmpr;
	}

	public void setCodigoEmpr(String codigoEmpr) {
		this.codigoEmpr = codigoEmpr;
	}

	public int getCodOrgProd() {
		return codOrgProd;
	}

	public void setCodOrgProd(int codOrgProd) {
		this.codOrgProd = codOrgProd;
	}

	public int getCodRamo() {
		return codRamo;
	}

	public void setCodRamo(int codRamo) {
		this.codRamo = codRamo;
	}

	public int getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(int codProduto) {
		this.codProduto = codProduto;
	}

}
