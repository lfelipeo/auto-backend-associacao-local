package br.com.segurossura.auto.dto;

/**
 * Classe DTO que representa um objeto do relacionamento entre assistências e
 * grupo corretor.
 * 
 * @author Renan Moreira
 */
public class GrupoCorretorAssistenciaDTO {

	private String nomeGrupoCorr;

	private String descrOpcao;

	private String status;

	private String codSubRamo;

	private String codigoEmpr;

	private String codigoOpcao;

	private String codGrupoCorr;

	private String codOrgProd;

	private String codRamo;

	private String codProduto;

	private String nomeProduto;

	private String codParam;

	public String getNomeGrupoCorr() {
		return nomeGrupoCorr;
	}

	public void setNomeGrupoCorr(String nomeGrupoCorr) {
		this.nomeGrupoCorr = nomeGrupoCorr;
	}

	public String getDescrOpcao() {
		return descrOpcao;
	}

	public void setDescrOpcao(String descrOpcao) {
		this.descrOpcao = descrOpcao;
	}

	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCodSubRamo() {
		return codSubRamo;
	}

	public void setCodSubRamo(String codSubRamo) {
		this.codSubRamo = codSubRamo;
	}

	public String getCodigoEmpr() {
		return codigoEmpr;
	}

	public void setCodigoEmpr(String codigoEmpr) {
		this.codigoEmpr = codigoEmpr;
	}

	public String getCodigoOpcao() {
		return codigoOpcao;
	}

	public void setCodigoOpcao(String codigoOpcao) {
		this.codigoOpcao = codigoOpcao;
	}

	public String getCodGrupoCorr() {
		return codGrupoCorr;
	}

	public void setCodGrupoCorr(String string) {
		this.codGrupoCorr = string;
	}

	public String getCodOrgProd() {
		return codOrgProd;
	}

	public void setCodOrgProd(String codOrgProd) {
		this.codOrgProd = codOrgProd;
	}

	public String getCodRamo() {
		return codRamo;
	}

	public void setCodRamo(String codRamo) {
		this.codRamo = codRamo;
	}

	public String getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(String codProduto) {
		this.codProduto = codProduto;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public String getCodParam() {
		return codParam;
	}

	public void setCodParam(String codParam) {
		this.codParam = codParam;
	}

}
