package br.com.segurossura.auto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.segurossura.auto.model.Produto;
import br.com.segurossura.auto.repository.ProdutoRepository;

@RestController
@RequestMapping({ "/produtos" })
public class ProdutoResource {
	@Autowired
	private ProdutoRepository repository;

	/**
	 * Método responsável por retornar todos os Produtos a partir de um Órgão
	 * Produtor, Ramo e Subramo.
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor para ser utilizado como parâmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo para ser utilizado como parâmetro de busca.
	 * @param codSubRamo
	 *            Código do Subramo para ser utilizado como parâmetro de busca.
	 * @return Retorna uma lista de Produtos que sejam do Órgão Produtor, Ramo e
	 *         SubRamo especificados.
	 * 
	 */
	@GetMapping
	public List<Produto> findAllProduto(int codOrgProd, int codRamo, int codSubRamo) {
		return repository.findAllProduto(codOrgProd, codRamo, codSubRamo);
	}

}
