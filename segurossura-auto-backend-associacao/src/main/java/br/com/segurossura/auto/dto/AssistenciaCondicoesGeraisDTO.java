package br.com.segurossura.auto.dto;

/**
 * Classe DTO que representa um objeto do relacionamento entre assistências e
 * condições gerais.
 * 
 * @author Renan Moreira
 */
public class AssistenciaCondicoesGeraisDTO {

	private String codigoOpcao;

	private String codOrgProd;

	private String codRamo;

	private String codProduto;

	private String codReport;

	private String dtInicVersao;

	private String ordemImpr;

	private String status;

	private String nomeProduto;

	private String descrOpcao;

	private String tituloCond;

	private String nomeCondicao;

	private String codParam;

	public String getCodOrgProd() {
		return codOrgProd;
	}

	public void setCodOrgProd(String codOrgProd) {
		this.codOrgProd = codOrgProd;
	}

	public String getCodRamo() {
		return codRamo;
	}

	public void setCodRamo(String codRamo) {
		this.codRamo = codRamo;
	}

	public String getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(String codProduto) {
		this.codProduto = codProduto;
	}

	public String getCodReport() {
		return codReport;
	}

	public void setCodReport(String codReport) {
		this.codReport = codReport;
	}

	public String getOrdemImpr() {
		return ordemImpr;
	}

	public void setOrdemImpr(String ordemImpr) {
		this.ordemImpr = ordemImpr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public String getDescrOpcao() {
		return descrOpcao;
	}

	public void setDescrOpcao(String descrOpcao) {
		this.descrOpcao = descrOpcao;
	}

	public String getTituloCond() {
		return tituloCond;
	}

	public void setTituloCond(String tituloCond) {
		this.tituloCond = tituloCond;
	}

	public String getDtInicVersao() {
		return dtInicVersao;
	}

	public void setDtInicVersao(String dtInicVersao) {
		this.dtInicVersao = dtInicVersao;
	}

	public String getCodigoOpcao() {
		return codigoOpcao;
	}

	public void setCodigoOpcao(String codigoOpcao) {
		this.codigoOpcao = codigoOpcao;
	}

	public String getNomeCondicao() {
		return nomeCondicao;
	}

	public void setNomeCondicao(String nomeCondicao) {
		this.nomeCondicao = nomeCondicao;
	}

	public String getCodParam() {
		return codParam;
	}

	public void setCodParam(String codParam) {
		this.codParam = codParam;
	}

}
