package br.com.segurossura.auto.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe referente à tabela de Opção Parâmetro.
 * 
 * @author Renan Moreira
 */
@Entity
@Table(name = "VT_OPCAO_PARAM")
public class CodParam {

	@EmbeddedId
	private CodParamPK id;

	@Column(name = "DESCR_OPCAO")
	private String descrOpcao;

	@Column(name = "DESL_S_N")
	private String condDesligado;
	
	public CodParamPK getId() {
		return id;
	}

	public void setId(CodParamPK id) {
		this.id = id;
	}

	public String getDescrOpcao() {
		return descrOpcao;
	}

	public void setDescrOpcao(String descrOpcao) {
		this.descrOpcao = descrOpcao;
	}

	public String getCondDesligado() {
		return condDesligado;
	}

	public void setCondDesligado(String condDesligado) {
		this.condDesligado = condDesligado;
	}

}
