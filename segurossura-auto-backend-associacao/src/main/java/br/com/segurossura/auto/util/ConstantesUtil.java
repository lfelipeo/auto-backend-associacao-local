package br.com.segurossura.auto.util;

/**
 * Classe de refencia de constantes do sistema;
 */
public class ConstantesUtil {

	public static final String DESTINO = "destino";

	public static final String MODO = "modo";

	public static final String USUARIO = "usuario";

	public static final Integer ENDOSSO_ZERO = 0;

	/*
	 * Número 0
	 */
	
	public static final int NUMERO_ZERO = 0;	
	
	/*
	 * Número 1
	 */
	
	public static final int NUMERO_UM = 1;

	/*
	 * Número 2
	 */
	
	public static final int NUMERO_DOIS = 2;

	/*
	 * Número 3
	 */
	
	public static final int NUMERO_TRES = 3;

	/*
	 * Número 4
	 */
	
	public static final int NUMERO_QUATRO = 4;

	/*
	 * Número 5
	 */
	
	public static final int NUMERO_CINCO = 5;

	/*
	 * Número 6
	 */
	
	public static final int NUMERO_SEIS = 6;

	/*
	 * Número 7
	 */
	
	public static final int NUMERO_SETE = 7;

	/*
	 * Número 8
	 */
	
	public static final int NUMERO_OITO = 8;

	/*
	 * Número 9
	 */
	
	public static final int NUMERO_NOVE = 9;

	/*
	 * Número 10
	 */
	
	public static final int NUMERO_DEZ = 10;

	/*
	 * Número 11
	 */
	
	public static final int NUMERO_ONZE = 11;

	private ConstantesUtil() {
	}

}
