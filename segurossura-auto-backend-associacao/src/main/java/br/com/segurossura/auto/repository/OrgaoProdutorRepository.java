package br.com.segurossura.auto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.segurossura.auto.model.OrgaoProdutor;
import br.com.segurossura.auto.model.OrgaoProdutorPK;


public interface OrgaoProdutorRepository extends CrudRepository<OrgaoProdutor, OrgaoProdutorPK> {
	
	/**
	
	Responsável por retornar todos os Órgãos Produtores.

	@return  Retorna todos os Órgãos Produtores.
	
	*/	
	
	@Query(value="SELECT * FROM VT_ORG_PROD WHERE cod_org_prod = 1 AND DT_DESL IS NULL ORDER BY NOME_ORG_PROD ASC", nativeQuery=true)
	public List<OrgaoProdutor> findAllOrgaoProdutor();
	
	
}
