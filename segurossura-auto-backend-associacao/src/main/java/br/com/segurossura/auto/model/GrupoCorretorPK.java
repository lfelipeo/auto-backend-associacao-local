package br.com.segurossura.auto.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Classe referente ao conjunto de colunas que representam a Primary Key da
 * tabela de Grupo Corretor.
 * 
 * @author Renan Moreira
 */
@Embeddable
public class GrupoCorretorPK implements Serializable {

	private static final long serialVersionUID = 6439419636604271919L;

	@Column(name = "COD_EMPR")
	private String codEmpr;

	@Column(name = "COD_GRUPO_CORR")
	private int codGrupoCorr;

	public String getCodEmpr() {
		return codEmpr;
	}

	public void setCodEmpr(String codEmpr) {
		this.codEmpr = codEmpr;
	}

	public int getCodGrupoCorr() {
		return codGrupoCorr;
	}

	public void setCodGrupoCorr(int codGrupoCorr) {
		this.codGrupoCorr = codGrupoCorr;
	}

}
