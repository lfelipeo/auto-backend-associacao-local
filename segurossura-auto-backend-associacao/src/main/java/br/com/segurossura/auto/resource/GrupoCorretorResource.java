package br.com.segurossura.auto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.segurossura.auto.model.GrupoCorretor;
import br.com.segurossura.auto.repository.GrupoCorretorRepository;

@RestController
@RequestMapping({ "/grupoCorretor" })
public class GrupoCorretorResource {
	@Autowired
	private GrupoCorretorRepository repository;

	/**
	 * 
	 * Metodo responsavel por retornar todos os Grupos Corretores.
	 * 
	 * @return Retorna uma lista de todos os Grupos Corretores.
	 * 
	 */
	@GetMapping
	public List<GrupoCorretor> findAllGrupoCorretor() {
		return repository.findAllGrupoCorretor();
	}

}
