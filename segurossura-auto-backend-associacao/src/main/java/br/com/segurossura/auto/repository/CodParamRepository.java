package br.com.segurossura.auto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.segurossura.auto.model.CodParam;
import br.com.segurossura.auto.model.CodParamPK;

@Repository
public interface CodParamRepository extends CrudRepository<CodParam, CodParamPK> {

	/**
	 * Responsável por retornar todas as Assistências.
	 * 
	 * @return Retorna todas as Assistências.
	 */
	@Query("select c from CodParam c WHERE c.id.codigoParam = 91 OR c.id.codigoParam = 93 AND c.condDesligado = 'N' ORDER BY c.descrOpcao  ASC")
	public List<CodParam> findAllAssistance();

}
