package br.com.segurossura.auto.security;

import static java.lang.Integer.parseInt;
import static java.util.Base64.getEncoder;
import static org.springframework.util.StringUtils.isEmpty;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.segurossura.auto.dto.UsuarioXMLDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
*
* Classe de Serviço de JWT;
*/

@Service
public class JwtService {

	private static final String CODIGO_EMPRESA = "codigoEmpresa";
	private static final String NUMERO_SPE = "numeroSPE";
	private static final String COD_CENTRAL_CORRETOR_COTACAO = "codCentralCorretorCotacao";
	private static final String COD_CENTRAL_CORRETOR = "codCentralCorretor";
	private static final String COD_GRUPO_CORRETOR = "codGrupoCorretor";
	private static final String TIPO_USUARIO = "tipoUsuario";
	private static final String TIPO_FUNCIONARIO = "tipoFuncionario";
	private static final String NOME_USUARIO = "nomeUsuario";
	private static final String COD_ORG_PROD = "codOrgProd";

	@Value("${jwt.expire.hours}")
	private Long expireHours;

	@Value("${jwt.token.secret}")
	private String plainSecret;

	private String encodedSecret;

	@PostConstruct
	protected void init() {
		this.encodedSecret = generateEncodedSecret(this.plainSecret);
	}

	/**
	 * Método reponsável por criptografar a chave secreta.
	 * 
	 * @param plainSecret
	 * @return
	 */
	private String generateEncodedSecret(String plainSecret) {
		if (isEmpty(plainSecret)) {
			throw new IllegalArgumentException("JWT secret cannot be null or empty.");
		}

		return getEncoder().encodeToString(this.plainSecret.getBytes());
	}

	/**
	 * Método reponsável por recuperar a data de expiração do token.
	 * 
	 * @return
	 */
	private Date getExpirationTime() {
		final Long expireInMilis = TimeUnit.HOURS.toMillis(expireHours);
		return new Date(expireInMilis + new Date().getTime());
	}

	public UsuarioXMLDTO getUser(String token) {
		return getUser(this.encodedSecret, token);
	}
	
	/**
	* Retorna o usuario de Segurança;
	* @param jwtUser Usuario de segurança;
	* @param token token de autenticação;
	*/
	public UsuarioXMLDTO getUser(String encodedSecret, String token) {
		final Claims claims = Jwts.parser().setSigningKey(encodedSecret).parseClaimsJws(token).getBody();
		final String idUsuWeb = claims.getSubject();
		UsuarioXMLDTO securityUser = new UsuarioXMLDTO();
		securityUser.setIdUsuWeb(parseInt(idUsuWeb));
		securityUser.setNomeUsuario((String) claims.get(NOME_USUARIO));
		securityUser.setTipoFuncionario((String) claims.get(TIPO_FUNCIONARIO));
		securityUser.setTipoUsuario((String) claims.get(TIPO_USUARIO));
		securityUser.setCodGrupoCorretor((String) claims.get(COD_GRUPO_CORRETOR));
		securityUser.setCodOrgPro((String) claims.get(COD_ORG_PROD));
		securityUser.setCodCentralCorretor((String) claims.get(COD_CENTRAL_CORRETOR));
		securityUser.setCodCentralCorretorCotacao((String) claims.get(COD_CENTRAL_CORRETOR_COTACAO));
		securityUser.setCodOrgPro((String) claims.get(COD_ORG_PROD));
		securityUser.setNumeroSPE((String) claims.get(NUMERO_SPE));
		securityUser.setCodigoEmpresa((String) claims.get(CODIGO_EMPRESA));

		return securityUser;
	}

	/**
	 * Método reponsável por recuperar o token.
	 * 
	 * @param encodedSecret
	 * @param jwtUser
	 * @return String
	 */
	public String getToken(String encodedSecret, UsuarioXMLDTO jwtUser) {
		return Jwts.builder().setId(UUID.randomUUID().toString()).setSubject(jwtUser.getIdUsuWeb().toString())
				.claim(TIPO_FUNCIONARIO, jwtUser.getTipoFuncionario()).claim(NOME_USUARIO, jwtUser.getNomeUsuario())
				.claim(TIPO_USUARIO, jwtUser.getTipoUsuario()).claim(COD_GRUPO_CORRETOR, jwtUser.getCodGrupoCorretor())
				.claim(COD_CENTRAL_CORRETOR, jwtUser.getCodCentralCorretor())
				.claim(COD_ORG_PROD, jwtUser.getCodOrgPro())
				.claim(COD_CENTRAL_CORRETOR_COTACAO, jwtUser.getCodCentralCorretorCotacao())
				.claim(COD_ORG_PROD, jwtUser.getCodOrgPro()).claim(NUMERO_SPE, jwtUser.getNumeroSPE())
				.claim(CODIGO_EMPRESA, jwtUser.getCodigoEmpresa()).setIssuedAt(Calendar.getInstance().getTime())
				.setExpiration(getExpirationTime()).signWith(SignatureAlgorithm.HS512, encodedSecret).compact();
	}

	/**
	 * Método reponsável por recuperar o token.
	 * 
	 * @param jwtUser
	 * @return String
	 */
	public String getToken(UsuarioXMLDTO jwtUser) {
		return getToken(this.encodedSecret, jwtUser);
	}

}
