package br.com.segurossura.auto.security;

import static java.util.Objects.isNull;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import javax.servlet.http.HttpServletRequest;
import br.com.segurossura.auto.util.ConstantesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import br.com.segurossura.auto.dto.LoginXMLDTO;
import br.com.segurossura.auto.dto.UsuarioXMLDTO;

/**
 * Classe do controle da Integração com a tela;
 */

@Controller
public class IntegracaoController {

	public static final Logger logger = LoggerFactory.getLogger(IntegracaoController.class);

	@Value("${url.frontend}")
	private String urlFrontEnd;

	@Value("${url.integracao}")
	private String urlIntegracao;

	@Value("${url.erro}")
	private String urlErro;

	@Autowired
	private JwtService jwtService;

	/**
	 * Método responsável por realizar a integração com o legado
	 * 
	 * @param request
	 * @return RedirectView
	 */
	@PostMapping("/integracao")
	public RedirectView integracaoLegado(HttpServletRequest request) {
		final RedirectView redirectView = new RedirectView();
		String xmlFile = request.getParameter(ConstantesUtil.USUARIO).trim();
		String destino = request.getParameter(ConstantesUtil.DESTINO).trim();

		try {
			xmlFile = DescriptografiaLoginXml.loginDescrypt(xmlFile.trim());

			if (isNull(xmlFile) || EMPTY.equals(xmlFile)) {
				redirectView.setUrl(this.urlFrontEnd + this.urlErro);
			} else {
				redirectView.setUrl(this.urlFrontEnd + "/" + destino);
			}
		} catch (Exception e) {
			logger.error("[IntegracaoController.integracaoLegado]", e);
			redirectView.setUrl(this.urlFrontEnd + this.urlErro);
		}

		return redirectView;
	}

	/**
	 * Método responsável por realizar o parse do xml do usuário para classe.
	 * 
	 * @param xmlFile
	 * @return UsuarioXMLDTO
	 */
	private UsuarioXMLDTO getUsuarioDTO(String xmlFile) {
		final XStream xstream = new XStream(new DomDriver());
		xstream.alias("rsagroup.vo.AutoAjustamentoVO", LoginXMLDTO.class);

		final LoginXMLDTO xmlLogin = (LoginXMLDTO) xstream.fromXML(xmlFile);
		final UsuarioXMLDTO usuario = new UsuarioXMLDTO();
		usuario.setMatrUsu(xmlLogin.getMatrUsu());
		usuario.setIdUsuWeb(xmlLogin.getIdUsuWeb());
		usuario.setNomeUsuario(xmlLogin.getNomeUsuario());
		usuario.setTipoUsuario(xmlLogin.getTipoUsuario());
		usuario.setLoginWeb(xmlLogin.getLoginWeb());
		usuario.setCodGrupoCorretor(xmlLogin.getCodGrupoCorretor());
		usuario.setCodOrgPro(xmlLogin.getCodOrgPro());
		usuario.setCodCentralCorretor(xmlLogin.getCodCentralCorretor());
		usuario.setCodCentralCorretorCotacao(xmlLogin.getCodCentralCorretorCotacao());
		usuario.setCodigoEmpresa(xmlLogin.getCodigoEmpresa());
		usuario.setNumeroSPE(xmlLogin.getNumeroSPE());

		return usuario;
	}

}
