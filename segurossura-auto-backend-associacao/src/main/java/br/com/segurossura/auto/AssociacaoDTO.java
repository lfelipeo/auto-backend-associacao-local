package br.com.segurossura.auto;

/**
 * Classe DTO que representa um objeto de associação.
 * 
 * Usado por AssociacaoGrupDTO
 * 
 * 
 * 
 * @author Renan Moreira
 * @since 26/10/2018
 */
public class AssociacaoDTO extends AssociacaoExtensaoDTO {

	/**
	 * Código Empresa
	 */

	private String codigoEmpr;

	/**
	 * Código Param.
	 */

	private int codigoParam;

	/**
	 * Código Opção
	 */

	private int codigoOpcao;

	/**
	 * Código Org. Opção
	 */

	private int codOrgProd;

	/**
	 * Código Ramo
	 */

	private int codRamo;

	/**
	 * Código Produto
	 */

	private int codProduto;

	/**
	 * Código Report
	 */

	private String codReport;

	public String getCodigoEmpr() {
		return codigoEmpr;
	}

	public void setCodigoEmpr(String codigoEmpr) {
		this.codigoEmpr = codigoEmpr;
	}

	public int getCodigoParam() {
		return codigoParam;
	}

	public void setCodigoParam(int codigoParam) {
		this.codigoParam = codigoParam;
	}

	public int getCodigoOpcao() {
		return codigoOpcao;
	}

	public void setCodigoOpcao(int codigoOpcao) {
		this.codigoOpcao = codigoOpcao;
	}

	public int getCodOrgProd() {
		return codOrgProd;
	}

	public void setCodOrgProd(int codOrgProd) {
		this.codOrgProd = codOrgProd;
	}

	public int getCodRamo() {
		return codRamo;
	}

	public void setCodRamo(int codRamo) {
		this.codRamo = codRamo;
	}

	public int getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(int codProduto) {
		this.codProduto = codProduto;
	}

	public String getCodReport() {
		return codReport;
	}

	public void setCodReport(String codReport) {
		this.codReport = codReport;
	}

}
