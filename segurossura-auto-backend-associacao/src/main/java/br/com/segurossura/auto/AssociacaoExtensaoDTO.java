package br.com.segurossura.auto;

import java.util.Date;

public class AssociacaoExtensaoDTO {


	/**
	 * Ordem Impr.
	 */

	private int ordemImpr;

	/**
	 * Status
	 */

	private String status;
	/**
	 * Dt. Inclusão
	 */

	private Date dtIncl;

	/**
	 * Usuario Inclusão
	 */

	private int usuIncl;

	/**
	 * Dt Alteração
	 */

	private String dtAlt;

	/**
	 * Hora Alteração
	 */

	private String horaAlt;

	/**
	 * Usuario Alteração
	 */

	private int usuAlt;

	public int getOrdemImpr() {
		return ordemImpr;
	}

	public void setOrdemImpr(int ordemImpr) {
		this.ordemImpr = ordemImpr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDtIncl() {
		return dtIncl;
	}

	public void setDtIncl(Date dtIncl) {
		this.dtIncl = dtIncl;
	}

	public int getUsuIncl() {
		return usuIncl;
	}

	public void setUsuIncl(int usuIncl) {
		this.usuIncl = usuIncl;
	}

	public String getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(String dtAlt) {
		this.dtAlt = dtAlt;
	}

	public String getHoraAlt() {
		return horaAlt;
	}

	public void setHoraAlt(String horaAlt) {
		this.horaAlt = horaAlt;
	}

	public int getUsuAlt() {
		return usuAlt;
	}

	public void setUsuAlt(int usuAlt) {
		this.usuAlt = usuAlt;
	}

}
