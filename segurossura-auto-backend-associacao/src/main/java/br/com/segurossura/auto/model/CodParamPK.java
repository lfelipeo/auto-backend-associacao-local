package br.com.segurossura.auto.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Classe referente ao conjunto de colunas que representam a Primary Key da
 * tabela de Opcao Parametro.
 * 
 * @author Renan Moreira
 */
@Embeddable
public class CodParamPK implements Serializable {

	private static final long serialVersionUID = -3505649258096048657L;

	@Column(name = "COD_EMPR")
	private String codigoEmpr;

	@Column(name = "COD_PARAM")
	private int codigoParam;

	@Column(name = "COD_OPCAO")
	private int codigoOpcao;

	public String getCodigoEmpr() {
		return codigoEmpr;
	}

	public void setCodigoEmpr(String codigoEmpr) {
		this.codigoEmpr = codigoEmpr;
	}

	public int getCodigoParam() {
		return codigoParam;
	}

	public void setCodigoParam(int codigoParam) {
		this.codigoParam = codigoParam;
	}

	public int getCodigoOpcao() {
		return codigoOpcao;
	}

	public void setCodigoOpcao(int codigoOpcao) {
		this.codigoOpcao = codigoOpcao;
	}

}
