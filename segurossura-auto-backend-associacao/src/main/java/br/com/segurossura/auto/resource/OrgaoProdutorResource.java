package br.com.segurossura.auto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.segurossura.auto.model.OrgaoProdutor;
import br.com.segurossura.auto.repository.OrgaoProdutorRepository;

@RestController
@RequestMapping({ "/orgaoProdutor" })
public class OrgaoProdutorResource {
	@Autowired
	private OrgaoProdutorRepository repository;

	/**
	 * 
	 * Método responsável por retornar todos os Órgãos Produtores.
	 * 
	 * @return Retorna uma lista de todos os Órgãos Produtores.
	 * 
	 */
	@GetMapping
	public List<OrgaoProdutor> findAllOrgaoProdutor() {
		return repository.findAllOrgaoProdutor();
	}

}
