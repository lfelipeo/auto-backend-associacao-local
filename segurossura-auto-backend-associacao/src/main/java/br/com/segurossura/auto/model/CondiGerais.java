package br.com.segurossura.auto.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe referente à tabela de Condições Gerais.
 * 
 * @author Renan Moreira
 */

@Entity
@Table(name = "VT_RELAT_COND_EMPR")
public class CondiGerais {

	@EmbeddedId
	private CondiGeraisPK id;

	@Column(name = "NOME_CONDICAO")
	private String nomeCond;

	public String getNomeCond() {
		return nomeCond;
	}

	public void setNomeCond(String nomeCond) {
		this.nomeCond = nomeCond;
	}

	public CondiGeraisPK getId() {
		return id;
	}

	public void setId(CondiGeraisPK id) {
		this.id = id;
	}

}
