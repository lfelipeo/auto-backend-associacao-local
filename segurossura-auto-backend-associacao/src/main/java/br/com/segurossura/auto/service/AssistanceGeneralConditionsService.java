package br.com.segurossura.auto.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.segurossura.auto.dto.AssistenciaCondicoesGeraisDTO;
import br.com.segurossura.auto.model.AssociacaoAssistCondi;
import br.com.segurossura.auto.repository.AssociacaoAssistCondiRepository;
import br.com.segurossura.auto.util.ConstantesUtil;

@Service
public class AssistanceGeneralConditionsService {

	@Autowired
	private AssociacaoAssistCondiRepository repository;

	/**
	 * Salva associação entre uma Assistência e uma Condição Geral.
	 * 
	 * @param associacao
	 *            Objeto que representa a associação entre uma Assistência e uma
	 *            Condição Geral
	 * @return Retorna o objeto de associação salvo em Banco.
	 * 
	 */

	public AssociacaoAssistCondi save(AssociacaoAssistCondi associacao) {
		return repository.save(associacao);
	}

	public List<AssistenciaCondicoesGeraisDTO> convertCollection(List<Object[]> collection) {

		List<AssistenciaCondicoesGeraisDTO> list = new ArrayList<>();

		for (Object[] obj : collection) {

			AssistenciaCondicoesGeraisDTO dto = new AssistenciaCondicoesGeraisDTO();
			dto.setCodOrgProd(String.valueOf(obj[ConstantesUtil.NUMERO_ZERO]));
			dto.setCodRamo(String.valueOf(obj[ConstantesUtil.NUMERO_UM]));
			dto.setCodProduto(String.valueOf(obj[ConstantesUtil.NUMERO_DOIS]));
			dto.setCodigoOpcao(String.valueOf(obj[ConstantesUtil.NUMERO_TRES]));
			dto.setDescrOpcao(String.valueOf(obj[ConstantesUtil.NUMERO_QUATRO]));
			dto.setCodReport(String.valueOf(obj[ConstantesUtil.NUMERO_CINCO]));
			dto.setNomeCondicao(String.valueOf(obj[ConstantesUtil.NUMERO_SEIS]));
			dto.setDtInicVersao(String.valueOf(obj[ConstantesUtil.NUMERO_SETE]));
			dto.setStatus(String.valueOf(obj[ConstantesUtil.NUMERO_OITO]));
			dto.setCodParam(String.valueOf(obj[ConstantesUtil.NUMERO_NOVE]));
						
			list.add(dto);
		}

		return list;
	}

	public List<AssistenciaCondicoesGeraisDTO> findAllAssociacaoByAssistencia(int codOpcao) {
		return convertCollection(repository.findAllAssociacaoByAssistencia(codOpcao));
	}

	public List<AssistenciaCondicoesGeraisDTO> findAllAssociacaoByProduto(int codOrgProd, int codProduto, int codRamo) {
		return convertCollection(repository.findAllAssociacaoByProduto(codOrgProd, codRamo, codProduto));
	}

	public List<AssistenciaCondicoesGeraisDTO> findAllAssociacaoCondicoes(int codOrgProd, int codRamo, int codProduto,
			int codOpcao, int codParam) {
		return convertCollection(repository.findAllAssociacaoCondicoes(codOrgProd, codRamo, codProduto, codOpcao, codParam));
	}

}
