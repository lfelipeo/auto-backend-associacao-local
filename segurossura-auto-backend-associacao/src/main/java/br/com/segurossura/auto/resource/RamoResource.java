package br.com.segurossura.auto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.segurossura.auto.model.Ramo;
import br.com.segurossura.auto.repository.RamoRepository;

@RestController
@RequestMapping({ "/ramos" })
public class RamoResource {
	@Autowired
	private RamoRepository repository;

	/**
	 * 
	 * Metodo responsavel por retornar todos os Ramos.
	 * 
	 * @return Retorna uma lista de todos os Ramos.
	 * 
	 */
	@GetMapping
	public List<Ramo> findAllRamo() {
		return repository.findAllRamo();
	}

}
