package br.com.segurossura.auto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.segurossura.auto.model.AssociacaoAssistCondi;
import br.com.segurossura.auto.model.AssociacaoAssistCondiPK;

/**
 * Classe reponsável por obter os dados da tabela de relacionamento entre
 * assistências e condições gerais.
 * 
 * @author Renan Moreira
 */
@Repository
public interface AssociacaoAssistCondiRepository extends
		CrudRepository<AssociacaoAssistCondi, AssociacaoAssistCondiPK> {

	@SuppressWarnings("unchecked")
	AssociacaoAssistCondi save(AssociacaoAssistCondi assistCondi);

	/**
	 * Responsável por retornar todas as associações entre Condição Geral e
	 * Assistência a partir de um Produto.
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parâmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parâmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parâmetro de busca.
	 * 
	 * @return Retorna todas as associações entre Condição Geral e Assistência a
	 *         partir do Órgão Produtor, Ramo e Produto especificados.
	 */
	@Query(value = "select TB.*,"
			+ "PT.TITULO_COND ,OP.DESCR_OPCAO from TB_OPCA_PARA_COND_TEXT TB "
			+ "JOIN VT_OPCAO_PARAM OP "
			+ "ON OP.COD_PARAM = 91 AND OP.COD_OPCAO = TB.COD_OPCAO"
			+ " JOIN VT_PROD_COND_TEXTO PT " + "ON PT.COD_RAMO = TB.COD_RAMO "
			+ "AND PT.COD_PRODUTO = TB.COD_PRODUTO "
			+ "AND PT.COD_ORG_PROD = TB.COD_ORG_PROD "
			+ "AND PT.COD_REPORT = TB.COD_REPORT "
			+ "AND PT.DT_INIC_VERSAO = TB.DT_INIC_VERSAO "
			+ "AND PT.ORDEM_IMPR = TB.ORDEM_IMPR"
			+ " where  TB.COD_ORG_PROD = :codOrgProd  "
			+ "AND TB.COD_RAMO =:codRamo "
			+ "AND  TB.COD_PRODUTO = :codProduto", nativeQuery = true)
	public List<AssociacaoAssistCondi> findAllAssociacao(
			@Param("codOrgProd") int codOrgProd, 
			@Param("codRamo") int codRamo,
			@Param("codProduto") int codProduto);

	/**
	 * Responsável por retornar uma associação entre Condição Geral e
	 * Assistência a partir de um id.
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parâmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parâmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parâmetro de busca.
	 * @param codReport
	 *            Código do Report a ser utilizado como parâmetro de busca.
	 * @param codOpcao
	 *            Código da assistência a ser utilizado como parâmetro de busca.
	 * 
	 * @return Retorna uma associação entre Condição Geral e Assistência a
	 *         partir do id.
	 */
	@Query(value = 
		      "SELECT * "
		      + " FROM TB_OPCA_PARA_COND_TEXT TB "
		      + "WHERE 1 = 1 "
		      + "AND TB.COD_ORG_PROD 	= :codOrgProd "
		      + "AND TB.COD_RAMO 		= :codRamo "
		      + "AND TB.COD_OPCAO 		= :codOpcao "
		      + "AND TB.COD_PRODUTO 	= :codProduto "
		      + "AND TB.COD_PARAM 		= :codParam "
		      + "AND TB.COD_REPORT		= :codReport", nativeQuery = true)
	public AssociacaoAssistCondi findById(
			@Param("codOrgProd") int codOrgProd,
			@Param("codRamo") int codRamo,
			@Param("codProduto") int codProduto,
			@Param("codReport") int  codReport,
			@Param("codOpcao") int codOpcao,
			@Param("codParam") int codParam);

	/**
	 * Responsável por retornar todas as associações entre Condição Geral e
	 * Assistência a partir de um Produto.
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parâmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parâmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parâmetro de busca.
	 * 
	 * @return Retorna todas as associações entre Condição Geral e Assistência a
	 *         partir do Órgão Produtor, Ramo e Produto especificados.
	 */
	@Query(value = "select "
			+ "TB.COD_ORG_PROD,TB.COD_OPCAO,TB.COD_RAMO, "
			+ "TB.COD_PRODUTO,TB.DT_INIC_VERSAO, "
			+ "TB.STATUS,P.NOME_PRODUTO, "
			+ "OP.DESCR_OPCAO from TB_OPCA_PARA_COND_TEXT TB JOIN VT_PRODUTO P "
			+ "ON P.COD_RAMO = TB.COD_RAMO "
			+ "AND TB.COD_PRODUTO = P.COD_PRODUTO "
			+ "AND P.COD_ORG_PROD = TB.COD_ORG_PROD "
			+ "JOIN VT_OPCAO_PARAM OP " + "ON OP.COD_PARAM = 91 "
			+ "AND OP.COD_OPCAO = TB.COD_OPCAO "
			+ " where TB.COD_ORG_PROD = :codOrgProd "
			+ "AND TB.COD_RAMO =:codRamo "
			+ "AND  TB.COD_PRODUTO = :codProduto", nativeQuery = true)
	public List<Object[]> findAllAssociacaoByProduto(
			@Param("codOrgProd") int codOrgProd, @Param("codRamo") int codRamo,
			@Param("codProduto") int codProduto);

	/**
	 * Responsável por retornar todas as associações entre Condição Geral e
	 * Assistência a partir de uma Assistência.
	 * 
	 * @param codOpcao
	 *            Código da Assistência ser utilizada como parâmetro de busca.
	 * 
	 * @return Retorna todas as associações entre Condição Geral e Assistência a
	 *         partir partir de uma Assistência.
	 */
	@Query(value = 
			  "select " + "TB.COD_ORG_PROD,TB.COD_OPCAO, "
			+ "TB.COD_RAMO,TB.COD_PRODUTO,"
			+ "TB.DT_INIC_VERSAO,TB.STATUS,P.NOME_PRODUTO, "
			+ "OP.DESCR_OPCAO from TB_OPCA_PARA_COND_TEXT TB "
			+ "JOIN VT_PRODUTO P ON P.COD_RAMO = TB.COD_RAMO "
			+ "AND TB.COD_PRODUTO = P.COD_PRODUTO "
			+ "AND P.COD_ORG_PROD = TB.COD_ORG_PROD "
			+ "JOIN VT_OPCAO_PARAM OP " + "ON OP.COD_PARAM = 91 "
			+ "AND OP.COD_OPCAO = TB.COD_OPCAO "
			+ " where TB.COD_OPCAO= :codOpcao", nativeQuery = true)
	public List<Object[]> findAllAssociacaoByAssistencia(
			@Param("codOpcao") int codOpcao);

	/**
	 * Responsável por retornar todos as condições gerais pelo produto e
	 * assistência.
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parâmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parâmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parâmetro de busca.
	 * @param codOpcao
	 *            Código da assistência a ser utilizado como parâmetro de busca.
	 * 
	 * @return Retorna uma associação entre Condição Geral e Assistência a
	 *         partir do Produto e Assistência.
	 */
	@Query(value = 
			      "SELECT DISTINCT "
			      + "TB.COD_ORG_PROD,"
			      + "TB.COD_RAMO, "
			      + "TB.COD_PRODUTO, "
			      + "TB.COD_OPCAO, "
			      + "OP.DESCR_OPCAO, "
			      + "RCE.COD_REPORT, "
			      + "RCE.NOME_CONDICAO, "
			      + "RCE.DT_INIC_VERSAO, "
			      + "TB.STATUS, "
			      + "TB.COD_PARAM  "
			      + "FROM TB_OPCA_PARA_COND_TEXT TB INNER JOIN VT_RELAT_COND_EMPR RCE ON RCE.COD_EMPR = TB.COD_EMPR AND RCE.COD_RAMO = TB.COD_RAMO"
			      + " AND RCE.COD_REPORT = TB.COD_REPORT "
			      //+ "AND RCE.DT_INIC_VERSAO = TB.DT_INIC_VERSAO "
			      + " INNER JOIN VT_OPCAO_PARAM OP ON OP.COD_PARAM = TB.COD_PARAM "
			      + " AND OP.COD_OPCAO = TB.COD_OPCAO "
			      + "WHERE 1 = 1 "
			      + "AND RCE.DESL_S_N = 'N' "
			      + "AND TB.COD_ORG_PROD 	= :codOrgProd "
			      + "AND TB.COD_RAMO 		= :codRamo "
			      + "AND TB.COD_OPCAO 		= :codOpcao "
			      + "AND TB.COD_PRODUTO 	= :codProduto "
			      + "AND TB.COD_PARAM 		= :codParam", nativeQuery = true)
	public List<Object[]> findAllAssociacaoCondicoes(
			@Param("codOrgProd") int codOrgProd, @Param("codRamo") int codRamo, 
			@Param("codProduto") int codProduto, @Param("codOpcao") int codOpcao, 
			@Param("codParam") int codParam);

}
