package br.com.segurossura.auto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.segurossura.auto.AssociacaoDTO;
import br.com.segurossura.auto.dto.AssistenciaCondicoesGeraisDTO;
import br.com.segurossura.auto.model.AssociacaoAssistCondi;
import br.com.segurossura.auto.model.AssociacaoAssistCondiPK;
import br.com.segurossura.auto.repository.AssociacaoAssistCondiRepository;
import br.com.segurossura.auto.service.AssistanceGeneralConditionsService;
import br.com.segurossura.auto.util.ConstantesUtil;

@RestController
@RequestMapping({ "/associacaoAssistCondi" })
public class AssociacaoAssistCondiResouce {

	@Autowired
	private AssociacaoAssistCondiRepository repository;

	@Autowired
	private AssistanceGeneralConditionsService service;

	/**
	 * 
	 * Método responsável por salvar a associação entre Condição Geral e
	 * Assistência.
	 * 
	 * @param associaDTO
	 *            Objeto DTO que recebe as informações da Request, inclui em
	 *            seus atributos e é utilizado para gerar o objeto de
	 *            associação.
	 * 
	 * @return Retorna o status Created após salvar o objeto de associação no
	 *         Banco de Dados.
	 * 
	 */

	@PostMapping({ "/salvar" })
	public ResponseEntity<Object> saveAssociacao2(@RequestBody AssociacaoDTO associaDTO) {

		AssociacaoAssistCondi associa = new AssociacaoAssistCondi();

		associa.setId(new AssociacaoAssistCondiPK(associaDTO.getCodigoOpcao(), associaDTO.getCodOrgProd(),
				associaDTO.getCodRamo(), associaDTO.getCodProduto(), associaDTO.getCodReport(),
				associaDTO.getOrdemImpr(), associaDTO.getCodigoParam()));

		associa.setStatus("S");

		repository.save(associa);

		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	/**
	 * 
	 * Método responsável por retornar todas as associações entre uma Condição
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parãmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parãmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parãmetro de busca.
	 * 
	 * @return Retorna todas as associações entre uma Condição Geral e
	 *         Assistência a partir do Produto especificado.
	 * 
	 */

	@GetMapping({ "/consultasAssistCondi" })
	public List<AssociacaoAssistCondi> findAllAssociacao(int codOrgProd, int codRamo, int codProduto) {
		return repository.findAllAssociacao(codOrgProd, codRamo, codProduto);
	}

	/**
	 * 
	 * Método responsável por retornar todas as associações entre uma Condição A
	 * partir de um Produto.
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parãmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parãmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parãmetro de busca.
	 * 
	 * @return Retorna todas as associações entre uma Condição Geral e
	 *         Assistência a partir do Produto especificado.
	 * 
	 */
	@GetMapping({ "/consultasAssistenciaByProduto" })
	public List<AssistenciaCondicoesGeraisDTO> findAllAssociacaoByProduto(int codOrgProd, int codProduto, int codRamo) {
		return service.findAllAssociacaoByProduto(codOrgProd, codProduto, codRamo);
	}

	/**
	 * 
	 * Método responsável por retornar todas as associações entre uma Condição A
	 * partir de uma Assistencia.
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parãmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parãmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parãmetro de busca.
	 * 
	 * @return Retorna todas as associações entre uma Condição Geral e
	 *         Assistência a partir do Produto especificado.
	 * 
	 */
	@GetMapping({ "/consultasAssistenciaByAssistencia" })
	public List<AssistenciaCondicoesGeraisDTO> findAllAssociacaoByAssistencia(int codOpcao) {
		return service.findAllAssociacaoByAssistencia(codOpcao);
	}

	/**
	 * 
	 * Método responsável por retornar todas as Assistencias
	 * 
	 * @param codOrgProd
	 *            Código do Órgão Produtor a ser utilizado como parãmetro de
	 *            busca.
	 * @param codRamo
	 *            Código do Ramo a ser utilizado como parãmetro de busca.
	 * @param codProduto
	 *            Código do Produto a ser utilizado como parãmetro de busca.
	 * 
	 * @return Retorna todas as Assistencias.
	 * 
	 */
	@GetMapping({ "/consultasAssistencia" })
	public List<AssistenciaCondicoesGeraisDTO> findAllAssociacaoCondicoes(int codOrgProd, int codRamo, int codProduto,
			int codOpcao, int codParam) {
		return service.findAllAssociacaoCondicoes(codOrgProd, codRamo, codProduto, codOpcao, codParam);
	}

	/**
	 * 
	 * Método responsável pela alteração dos estados das Associações;
	 * 
	 * @param AssistenciaCondicoesGeraisDTO
	 *            recebe a associação para ser buscada no banco;
	 * 
	 * @return Retorna a Associação alterada;
	 * 
	 */
	
	@PutMapping({ "/alterar" })
	public @ResponseBody AssociacaoAssistCondi updateNote(
			@RequestParam Integer codRamo,
			@RequestParam Integer codProduto,
			@RequestParam("codOpcao") Integer codOpcao,
			@RequestParam("codReport") Integer codReport,
			@RequestParam("codParam") Integer codParam,
			@RequestBody AssociacaoAssistCondi assist) {

		// Foi chumbado o número 1 no Orgão Produtor pois foi acordado
		// que sempre será Matriz (1). 05/12/2018
		// Luiz Oliveira.
				
		AssociacaoAssistCondi associao = repository.findById(ConstantesUtil.NUMERO_UM, codRamo, codProduto, codReport, codOpcao, codParam);
		
		if ("N".equals(associao.getStatus())) {
			associao.setStatus("S");
		} else {
			associao.setStatus("N");
		}

		return repository.save(associao);
	}
	
}
