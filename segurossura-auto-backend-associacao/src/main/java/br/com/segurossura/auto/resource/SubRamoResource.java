package br.com.segurossura.auto.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.com.segurossura.auto.model.SubRamo;
import br.com.segurossura.auto.repository.SubRamoRepository;

@RestController
@RequestMapping({"/subRamos"})
public class SubRamoResource {
	@Autowired
	private SubRamoRepository repository;
	
	/**
	 * 
	 * Método responsável por retornar todos os Subramos a partir de um Ramo.
	 * 
	 * @param codRamo
	 *            Código do Ramo para ser utilizado como parâmetro de busca.
	 * @return Retorna uma lista de Subramos que sejam do ramo especificado.
	 * 
	 */
	@GetMapping
	public List<SubRamo> findAllSubRamo(int codRamo){		
		return repository.findAllSubRamo(codRamo);
	}

}
