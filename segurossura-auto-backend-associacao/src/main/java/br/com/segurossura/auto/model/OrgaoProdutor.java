package br.com.segurossura.auto.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Classe referente à tabela de Órgão Produtor.
 * 
 * @author Renan Moreira
 */
@Entity
@Table(name = "VT_ORG_PROD")
public class OrgaoProdutor {

	@EmbeddedId
	private OrgaoProdutorPK id;

	@Column(name = "NOME_ORG_PROD")
	private String nomeOrgProd;

	public OrgaoProdutorPK getId() {
		return id;
	}

	public void setId(OrgaoProdutorPK id) {
		this.id = id;
	}

	public String getNomeOrgProd() {
		return nomeOrgProd;
	}

	public void setNomeOrgProd(String nomeOrgProd) {
		this.nomeOrgProd = nomeOrgProd;
	}

}
