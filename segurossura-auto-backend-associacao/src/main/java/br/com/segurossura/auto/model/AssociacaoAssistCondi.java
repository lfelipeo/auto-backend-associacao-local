package br.com.segurossura.auto.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Classe referente à tabela de associação entre Condição Geral e Assistência
 * (Opção Parâmetro).
 * 
 * @author Renan Moreira
 */
@Entity
@Table(name = "TB_OPCA_PARA_COND_TEXT")
public class AssociacaoAssistCondi  {
	
	@EmbeddedId                         
	private AssociacaoAssistCondiPK id; 

	@Column(name = "STATUS")
	private String status;

	@Column(name = "DT_INCL")
	private String dtIncl;

	@Column(name = "USU_INCL")
	private Integer usuIncl;

	@Column(name = "DT_ALT")
	private String dtAlt;

	@Column(name = "HORA_ALT")
	private String horaAlt;

	@Column(name = "USU_ALT")
	private Integer usuAlt;
	
	@Column(name = "DT_INIC_VERSAO")
	private Date dtInicVersao;
	
	/**
	*  Construtor em branco para a instanciação da classe vaiza.
	*/
	public AssociacaoAssistCondi() {
		
	}
	/**
	 Construtor com todos os atributos;
	*
	*@param  id a Primary Key da tabela da relação Assistencia Condição;
	*@param  status
	*@param  dtIncl
	*@param  dtAlt
	*@param  horaAlt
	*@param  usuAlt
	*/
	
	public AssociacaoAssistCondi(AssociacaoAssistCondiPK  id, String status, String dtIncl, int usuIncl, String dtAlt,
			String horaAlt, int usuAlt) {
		super();
		this.id = id;
		this.status = status;
		this.dtIncl = dtIncl;
		this.usuIncl = usuIncl;
		this.dtAlt = dtAlt;
		this.horaAlt = horaAlt;
		this.usuAlt = usuAlt;
	}

	public AssociacaoAssistCondiPK getId() {
		return id;
	}

	public void setId(AssociacaoAssistCondiPK id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDtIncl() {
		return dtIncl;
	}

	public void setDtIncl(String dtIncl) {
		this.dtIncl = dtIncl;
	}

	public Integer getUsuIncl() {
		return usuIncl;
	}

	public void setUsuIncl(Integer usuIncl) {
		this.usuIncl = usuIncl;
	}

	public String getDtAlt() {
		return dtAlt;
	}

	public void setDtAlt(String dtAlt) {
		this.dtAlt = dtAlt;
	}

	public String getHoraAlt() {
		return horaAlt;
	}

	public void setHoraAlt(String horaAlt) {
		this.horaAlt = horaAlt;
	}

	public Integer getUsuAlt() {
		return usuAlt;
	}

	public void setUsuAlt(Integer usuAlt) {
		this.usuAlt = usuAlt;
	}

	// get da data
	
	public Date getDtInicVersao() {
		return dtInicVersao;
	}
	
	// set da data
	
	public void setDtInicVersao(Date dtInicVersao) {
		this.dtInicVersao = dtInicVersao;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dtAlt == null) ? 0 : dtAlt.hashCode());
		result = prime * result + ((dtIncl == null) ? 0 : dtIncl.hashCode());
		result = prime * result + ((horaAlt == null) ? 0 : horaAlt.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + usuAlt;
		result = prime * result + usuIncl;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		boolean returnValue = true;
		
		if (this == obj)
			returnValue = true;
		
		if (obj == null)
			returnValue = false;
		
		if (getClass() != obj.getClass())
			returnValue = false;
		
		AssociacaoAssistCondi other = (AssociacaoAssistCondi) obj;
		if (dtAlt == null) {
			if (other.dtAlt != null)
				returnValue = false;
		} else if (!dtAlt.equals(other.dtAlt))
			returnValue = false;
		if (dtIncl == null) {
			if (other.dtIncl != null)
				returnValue = false;
		} else if (!dtIncl.equals(other.dtIncl))
			returnValue = false;
		if (horaAlt == null) {
			if (other.horaAlt != null)
				returnValue = false;
		} else if (!horaAlt.equals(other.horaAlt))
			returnValue = false;
		if (id == null) {
			if (other.id != null)
				returnValue = false;
		} else if (!id.equals(other.id))
			returnValue = false;
		if (status == null) {
			if (other.status != null)
				returnValue = false;
		} else if (!status.equals(other.status))
			returnValue = false;
		if (usuAlt != other.usuAlt)
			returnValue = false;
		if (usuIncl != other.usuIncl)
			returnValue = false;
		
		return returnValue;
	}

}