package br.com.segurossura.auto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import br.com.segurossura.auto.model.Ramo;

public interface RamoRepository extends CrudRepository<Ramo, Integer> {
	
	/**
	
	Método responsável por retornar todas os Ramos.
			
	@return  Retorna todas os Ramos.
	
	*/
	
	@Query(value="SELECT * FROM VT_RAMO WHERE DT_DESL IS NULL AND COD_RAMO IN (310,530) ORDER BY NOME_RAMO  ASC", nativeQuery=true)
	public List<Ramo> findAllRamo();

}
